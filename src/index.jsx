import React from "react";
import { Provider } from "react-redux";
import { render } from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import * as ServiceWorker from "./ServiceWorker";

//Custom
import Routes from "./Route";
import reducers from "./store/reducers";

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

const App = (
	<Provider store = {store}>
		<BrowserRouter basename = "/">
			<Route path = "/" component = {Routes} />
		</BrowserRouter>
	</Provider>
);

render(App, document.querySelector("#root"));

ServiceWorker.register();
