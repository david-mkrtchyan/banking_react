import React from "react";
import { Redirect, NavLink } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";

//Styles
//
import "Style/Libs/framework.css";
import "Style/Libs/style.css";
//
import "./styled.scss";

//Components
import { Content } from "UI/Content";
import Modal from "UI/Modal";
import SignUp from "Container/Auth/SignUp";

const SignUpPage = ( { 
	isLoged, 
	headerHeight
} ) => (
	<>
		{isLoged && <Redirect to = "/" />}
		<main className = "SignUp">
			<Content height = {headerHeight}>
				<Modal 
					width = {300} 
					title = "SignUp"
					LogoWidth = "105px"
				>
					<SignUp />
					<Content>
						<NavLink to = "/login">
							Login
						</NavLink>
					</Content>
				</Modal>
			</Content>
		</main>
	</>
);

SignUpPage.propTypes = {
	headerHeight: propTypes.number,
	isLoged: propTypes.bool
};

const mapState = state => ({
	isLoged: state.LogIn.isLoged,
	headerHeight: state.LayoutSize.headerSize
});

export default connect( mapState )( SignUpPage );
