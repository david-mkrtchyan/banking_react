import React from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { Redirect, NavLink } from "react-router-dom";

//Components
import { Content } from "UI/Content";
import Modal from "UI/Modal";
import LogIn from "Container/Auth/LogIn";

//Styles
import "./styled.scss";

const LogInPage = ( {
	isLoged,
	headerHeight,
} ) => (
	<>
		{isLoged && <Redirect to = "/" />}
		<main className = "LogIn">
			<Content height = {headerHeight}>
				<Modal width = {300} title = "Login">
					<LogIn />
				</Modal>
			</Content>
		</main>
		
	</>
);

LogInPage.propTypes = {
	headerHeight: propTypes.number,
	isLoged: propTypes.bool
};

const mapStateToProps = state => ({
	headerHeight: state.LayoutSize.headerSize,
	isLoged: state.LogIn.isLoged,
	FormLogin: state.FormLogin
});

export default connect( mapStateToProps )( LogInPage );
