import React, { useEffect } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { Redirect } from "react-router-dom";

//Components
import { Content } from "UI/Content";
import Modal from "UI/Modal";
import VerifedPhone from "Container/Auth/VerifedPhone";
import { GetSession } from "Engine/BrowserStorage";

//Styles
import "./styled.scss";

const VerifedPhonePage = ( {
    isLoged,
	headerHeight
} ) => (
	<>
		{(isLoged || !GetSession("_rs")) && <Redirect to = "/" />}
		<main className = "VerifedPhone">
			<Content height = {headerHeight}>
				<Modal 
					width = {250} 
					title = "Pin"
					LogoWidth = "105px"
				>
					<VerifedPhone />
				</Modal>
			</Content>
		</main>
	</>
);

VerifedPhonePage.propTypes = {
	headerHeight: propTypes.number,
	isLoged: propTypes.bool
};

const mapStateToProps = state => ({
	headerHeight: state.LayoutSize.headerSize,
	isLoged: state.LogIn.isLoged
});

export default connect( mapStateToProps )( VerifedPhonePage );
