import React from "react";
import { connect} from "react-redux";
import { Redirect  } from "react-router-dom";
import { GetSession } from "BrowserStorage";

import FeedBack from "Container/FeedBack";
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import DataIncludes from "Container/DataIncludes";


//Components
import AccountsCard from "UI/AccountsCard";
import GroupContent from "UI/GroupContent";
import NewCurrency from "~c/NewCurrency";
import Message from "~c/Message";

import "./styles.scss";

const Home = ({ 
	isLoged,
	currency,
	FeedBackMessages
}) => {
	const mapAccountsCard = currency.map(item => (
		<AccountsCard
			valute = { item.name }
			money = { item.balance }
			key = { item.account_id }
		/>
	));

	return (
		<> 
		
			{ !GetSession("_w") ? 
				window.location.href =  "/welcome" 
			:
				<>
					{ !isLoged && <Redirect to = "/login" /> }
					<Header
						mainTitle = {window.location.pathname}
					/>
					<main className = "Home Main scrollbar contentBody">
						<DataIncludes>
							<GroupContent
								mainTitle = "Accounts"
								className = "HomeContainer"
							>
								{ mapAccountsCard }
								<NewCurrency />
							</GroupContent>
							<Message  />
							<FeedBack FeedBackMessages = {FeedBackMessages} />
						</DataIncludes>
					</main>
					<Footer />
				</>
			}
		</>
	);
}

const mapState = state => ({
	currency: state.Profil.currency,
	isLoged: state.LogIn.isLoged,
	FeedBackMessages: state.FeedBackMessages.messages
});

export default connect( mapState )( Home );
