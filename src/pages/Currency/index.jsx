import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Modal from "UI/Modal";
import BoxCurrency from "Container/Transfer/BoxCurrency";
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";

import { Currency, SendRefreshTokenUser } from "Action";
import { callPopUpMessage } from "UI/Messages";
import { GetTokenRefresh } from "BrowserStorage";

import "./style.scss";

const CurrencyPage = ({ 
	currency, 
	CheckUserCurrency,
	isLoged,
	SendRefreshTokenUser
}) => {
	const [loading, setLoading] = useState(false);

	const mapBoxCurrency = currency.map(item => (
		<BoxCurrency
			currency = { item.name }
			balance = { item.balance }
			key = { item.account_id }
			id = { item.account_id }
		/>
	));

	useEffect(() => {
		(async () => {
			const token = GetTokenRefresh("_t");
			token && await SendRefreshTokenUser(token);
			const message = await CheckUserCurrency();

			if (message.status >= 300) {
				(async () => {
					const feedBack = await message.json();
					callPopUpMessage.error( feedBack.detail );
				})();
			} else {
				setLoading(true);
			};
		})();
	}, []);

	return (
		<>
			{ !isLoged && <Redirect to = "/" /> } 
			<main className = "Currency contentBody">
				<Header
					mainTitle = {window.location.pathname}
				/>
				{ loading ? (
					<Modal to title = "Currency">
						{mapBoxCurrency}
					</Modal>
				) : null }
			</main>
			<Footer />
		</>
	);
};

const mapStateToProps = state => ({
	currency: state.Profil.currency,
	isLoged: state.LogIn.isLoged
});

const mapDispatchToProps = dispatch => ({
	CheckUserCurrency: () => dispatch(Currency.CheckUserCurrency()),
	SendRefreshTokenUser: token => dispatch(SendRefreshTokenUser(token))
});

export default connect( mapStateToProps, mapDispatchToProps )( CurrencyPage );
