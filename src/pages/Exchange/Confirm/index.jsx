import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import {ExchangeAction} from "Action";
import {Redirect, Link} from "react-router-dom";

//Components
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import ConfirmExchangeComponent from "~c/Exchange/Confirm";
import Button from "UI/Button";
import {callPopUpMessage} from "UI/Messages";

//Engine
import {Decoding} from "Engine/Encryption";
import {ParseDecodingJSON, ParseJSON} from "Engine";
import {GetSession} from "BrowserStorage";

//Styles
import "./styled.scss";

const ConfirmExchange = ({
													 isLoged,
													 match,
													 TransactionExchange,
													 ExchangeData,
													 ChangeAllExchangeForm
												 }) => {
	const [data, setData] = useState(null);
	const [choiceLink, setChoiceLink] = useState(false);

	useEffect(() => {
		const CheckDataBLL = () => {
			const dataEX = ParseJSON(Decoding(GetSession("_ex"), "_ex"));
			ChangeAllExchangeForm(dataEX);
			console.log(dataEX)
			return dataEX;
		};

		if (!CheckDataBLL()) {
			let LinkData;
			try {
				LinkData = Decoding(match.params.data, "data");
			} catch (e) {
				window.location.replace("/send")
			}
			setData(ParseDecodingJSON(LinkData));
			setChoiceLink(true);
		}
	}, []);

	const handleClickConfirm = async () => {
		const message = await TransactionExchange(choiceLink ? data : ExchangeData)

		if (message.status >= 300) {
			(async () => {
				const feedBack = await message.json();
				Object.keys(feedBack).forEach(item => callPopUpMessage.error(feedBack[item][0]));
			})();
		}
	};

	return (
		<>
			{!isLoged && <Redirect to="/login"/>}
			<Header
				mainTitle={match.path}
			/>
			<main className="Confirm Main scrollbar contentBody">
				<div className="WrapperConfirm">
					<ConfirmExchangeComponent
						ConfirmTitle="Please confirm Exchanging :"
						ConfirmToText="To:"
						Amount = {choiceLink ? (data && data.amount) : ExchangeData.amount}
						BaseCurrency = {choiceLink ? (data && data.base_currency) : ExchangeData.base_currency}
						SwapCurrency = {choiceLink ? (data && data.swap_currency) : ExchangeData.swap_currency}
						Price = {choiceLink ? (data && data.price) : ExchangeData.price}
					/>
					<Button
						Margin = "15px 0 0 0"
						Padding = "17px 68px"
						onClick = {handleClickConfirm}
					>
						<span className = "ConfirmButton"> CONFIRM </span>
					</Button>
					<br />
					<Link to = "/exchange" className = "BackLink">
							<span className = "NoBack">
									NO, go back and change.
							</span>{" "}
					</Link>
				</div>
			</main>
			<Footer/>
		</>
	);
}

const mapState = state => ({
	isLoged: state.LogIn.isLoged,
	ExchangeData: state.ExchangeData
});

const mapDispatch = dispatch => ({
	TransactionExchange: data => dispatch(ExchangeAction.TransactionExchange(data)),
	ChangeAllExchangeForm: data => dispatch(ExchangeAction.ChangeAllExchangeForm(data))
});

export default connect(mapState, mapDispatch)(ConfirmExchange);
