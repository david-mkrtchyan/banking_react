import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {ExchangeAction} from "Action";

//Components
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import ConfirmExchangeComponent from "~c/Exchange/Confirm";

//IMG
import CheckSuccess from "IMG/CheckSuccess.png";
import CheckError from "IMG/CheckError.png";

//Engine
import {Decoding} from "Engine/Encryption";
import {ParseDecodingJSON, ParseJSON} from "Engine";
import {GetSession, RemoveSession} from "BrowserStorage";

//Styles
import "./styled.scss";

const ConfirmationExchange = ({
																isLoged,
																match,
																ExchangeData,
																ChangeAllExchangeForm
															}) => {
	const [data, setData] = useState(null);
	const [choiceLink, setChoiceLink] = useState(false);

	useEffect(() => {
		const CheckDataBLL = () => {
			const dataEX = ParseJSON(Decoding(GetSession("_ex"), "_ex"));
			console.log(dataEX);
			ChangeAllExchangeForm(dataEX);
			return dataEX;
		};

		if (!CheckDataBLL()) {
			let LinkData;
			try {
				LinkData = Decoding(match.params.data, "data")
			} catch (e) {
				window.location.replace("/send")
			}
			setData(ParseDecodingJSON(LinkData));
			setChoiceLink(true);
		}
		return () => {
			RemoveSession("_ex");
		};
	}, []);

	return (
		<>
			{!isLoged && <Redirect to="/login"/>}
			<Header
				mainTitle={match.path}
			/>
			<main className="Confirm Main scrollbar contentBody">
				<div className="WrapperConfirm">
					<ConfirmExchangeComponent
						Mode="confirm"
						ConfirmToText="is exchanged to:"
						Amount = {choiceLink ? (data && data.amount) : ExchangeData.amount}
						BaseCurrency = {choiceLink ? (data && data.base_currency) : ExchangeData.base_currency}
						SwapCurrency = {choiceLink ? (data && data.swap_currency) : ExchangeData.swap_currency}
						Price = {choiceLink ? (data && data.price) : ExchangeData.price}
					/>
					<div className="imageBlock">
						{((choiceLink && (data && data.success)) || ExchangeData.success) ?
							<img className="check-mark" src={CheckSuccess} alt=""/>
							:
							<img className="check-mark" src={CheckError} alt=""/>
						}
					</div>
				</div>
			</main>
			<Footer/>
		</>
	);
}

const mapState = state => ({
	isLoged: state.LogIn.isLoged,
	ExchangeData: state.ExchangeData
});

const mapDispatch = dispatch => ({
	ChangeAllExchangeForm: data => dispatch(ExchangeAction.ChangeAllExchangeForm(data))
});


export default connect(mapState, mapDispatch)(ConfirmationExchange);
