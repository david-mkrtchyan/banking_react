import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import { Currency, ExchangeAction, SendRefreshTokenUser } from "Action";
import { GetTokenRefresh } from "BrowserStorage";

import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import LoadingWait from "HOC/LoadingWait";
import SelectAcount from "~c/SelectAcount";
import BaseCurrencyContainer from "Container/Exchange/BaseCurrencyContainer";
import SwapCurrencyContainer from "Container/Exchange/SwapCurrencyContainer";
import Button from "UI/Button";
import "./style.scss";
import {ChangeExchangeForm} from "../../store/actions/Exchange/Form";
import {ExchangeDataUrl} from "../../server/URL";

const Exchange = ({
						   CheckUserCurrency,
										ExchangeData,
						   currency,
							 ChangeExchangeForm,
						   SendRefreshTokenUser
					   }) => {
	const [ serverError, setServerError ] = useState(false);
	const [ currencySpanBase , setCurrencySpanBase ] = useState(0);
	const [ currencySpanSwap , setCurrencySpanSwap ] = useState(0);
	const [ loading, setLoading ] = useState(true);

	useEffect(() => {
		const CheckProfilCurrency = async () => {
			setLoading(true)
			const message = await CheckUserCurrency()

			if (message.status >= 300) {
				const token = GetTokenRefresh("_t");
				(async () => {
					await SendRefreshTokenUser(token);
					CheckProfilCurrency()
				})();
				return;
			}

			setLoading(false)
		};
		CheckProfilCurrency();
		// (async () => {
		//     const message = await CheckUserCurrency();
		//     if ( message.status >= 300 ) {
		//         setServerError(true)
		//     };
		// })();
	}, []);

	useEffect(() => {
		console.log(ExchangeData);
		!(currencySpanBase === null) && currency.length && ChangeExchangeForm( currency[currencySpanBase].name, "base_currency");
		!(currencySpanSwap === null) && currency.length && ChangeExchangeForm( currency[currencySpanSwap].name, "swap_currency");
	}, [currencySpanBase,currencySpanSwap]);

	const ReloadPage = () => {
		window.location.reload()
	};

	const changeCurrencyBase = async num => {
		setCurrencySpanBase(num);
	};

	const changeCurrencySwap = async num => {
		setCurrencySpanSwap(num);
	};

	return (
		<LoadingWait loading = {loading}>
			<Header
				mainTitle = { "/exchange" }
			/>
			<main className = "ExchangePage Main contentBody">
				{ serverError ?
					(
						<div className = "ServerPathError">
							<p className = "TextServerPath">
								Server Path Error
							</p>
							<div className = "ServerPathErrorReload" onClick = {ReloadPage}>
								Please Reload this Page
							</div>
						</div>
					) :
					(
						<>
							<div className = "SelectAccountTo">
								<SelectAcount
											  Currency = { currency }
											  changeCurrency = { changeCurrencyBase }
								/>
							</div>
							<div className = "AmountCurrency">
								<BaseCurrencyContainer
									balance = { currency.length ? currency[currencySpanBase].balance : 0 }
									valute = { currency.length ? currency[currencySpanBase].name : "USD" }
									name = "amount"
								/>
							</div>
							<div className = "SelectAccountFrom">
								<SelectAcount
									Currency = { currency }
									changeCurrency = { changeCurrencySwap }
								/>
							</div>
							<div className = "AmountCurrency SwapCurrencyContainer">
								<SwapCurrencyContainer
									balance = { currency.length ? currency[currencySpanBase].balance : 0 }
									valute = { currency.length ? currency[currencySpanSwap].name : "USD" }
									name = "price"
								/>
							</div>
						</>
					)
				}
			</main>
			<Footer />
		</LoadingWait>
	);
}

const mapState = state => ({
	currency: state.Profil.currency,
	ExchangeData: state.ExchangeData,
});

const mapDispatch = dispatch => ({
	CheckUserCurrency: () => dispatch(Currency.CheckUserCurrency()),
	ChangeExchangeForm: ( data, name ) => dispatch( ExchangeAction.ChangeExchangeForm( data, name ) ),
	SendRefreshTokenUser: token => dispatch(SendRefreshTokenUser(token))
});

export default connect( mapState, mapDispatch )( Exchange );
