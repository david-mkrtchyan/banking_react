import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { GetSession } from "BrowserStorage";
import LogoWelcome from "~c/includes/LogoWelcome";
import { SetSession } from "BrowserStorage";

import "./styled.scss";

const Welcome = () => {
    useEffect(() => {
        SetSession("_w", true);         
        const time = setTimeout(() => window.location.replace("/logIn"), 2000);
        return () => {
            clearTimeout(time);
        }
    },[]);

    return (
        <>
            { GetSession("_w") && <Redirect to = "/" /> }
            <main className = "WelcomePage">
                <div className = "WelcomeContent">
                    <LogoWelcome />
                </div>
            </main>
        </>
    );
}

export default Welcome;
