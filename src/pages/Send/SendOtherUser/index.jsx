import React, { useEffect, useState } from "react";
import { connect } from "react-redux"; 

import { Currency, SendAnother, SendRefreshTokenUser } from "Action";
import { GetTokenRefresh } from "BrowserStorage";

import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import LoadingWait from "HOC/LoadingWait";
import SelectAcount from "~c/SelectAcount";
import SendToCurrency from "Container/Transfer/CurrencySend";

import "./styled.scss";

const SendOtherUser = ({
    CheckUserCurrency,
    currency,
    ChangeAnotherUserForm,
    SendRefreshTokenUser
}) => {
    const [ serverError, setServerError ] = useState(false);
    const [ currencySpan , setCurrencySpan ] = useState(0);
    const [ loading, setLoading ] = useState(true);

    useEffect(() => {
        const CheckProfilCurrency = async () => {
			setLoading(true)
            const message = await CheckUserCurrency()
            
			if (message.status >= 300) {
				const token = GetTokenRefresh("_t");
				(async () => {
					await SendRefreshTokenUser(token);
					CheckProfilCurrency()
				})();
				return;
			}

			setLoading(false)
		};
		CheckProfilCurrency();
        // (async () => {
        //     const message = await CheckUserCurrency();
        //     if ( message.status >= 300 ) {
        //         setServerError(true)
        //     };
        // })();
    }, []);

    useEffect(() => {
        !(currencySpan === null) && currency.length && ChangeAnotherUserForm( currency[currencySpan].account_id, "account_id");
        !(currencySpan === null) && currency.length && ChangeAnotherUserForm( currency[currencySpan].name, "currency");
    }, [currencySpan, currency]);

    useEffect(() => setCurrencySpan(0), [currency]);

    const ReloadPage = () => {
        window.location.reload()
    };
    const changeCurrency = async num => {
        setCurrencySpan(num);
    };

    return (
        <LoadingWait loading = {loading}>
            <Header
                mainTitle = { "/send" }
            />
            <main className = "SendOtherUserPage Main contentBody">
                { serverError ?
                    (
                        <div className = "ServerPathError">
                            <p className = "TextServerPath">
                                Server Path Error
                            </p>
                            <div className = "ServerPathErrorReload" onClick = {ReloadPage}>
                                Please Reload this Page
                            </div>
                        </div>
                    ) :
                    (
                       <>
                            <SelectAcount
                                Currency = { currency }
                                changeCurrency = { changeCurrency }
                            />
                            <div className = "AmountCurrency">
                                <SendToCurrency
                                    balance = { currency.length ? currency[currencySpan].balance : 0 }
                                    valute = { currency.length ? currency[currencySpan].name : "USD" }
                                />
                            </div>
                        </>
                    )
                }
            </main>
            <Footer />
        </LoadingWait>
    );
}

const mapState = state => ({
    currency: state.Profil.currency,
});

const mapDispatch = dispatch => ({
    CheckUserCurrency: () => dispatch(Currency.CheckUserCurrency()),
    ChangeAnotherUserForm: ( data, name ) => dispatch( SendAnother.ChangeAnotherUserForm( data, name ) ),
    SendRefreshTokenUser: token => dispatch(SendRefreshTokenUser(token))
});

export default connect( mapState, mapDispatch )( SendOtherUser );
