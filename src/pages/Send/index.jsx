import React from "react";
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";

//Components
import GroupContent from "UI/GroupContent";
import AccountsCard from "UI/AccountsCard";

//Styled
import "./styled.scss";

const Send = () => (
    <>
		<Header
			mainTitle = { window.location.pathname }
		/>
        <main className = "SendPage Main contentBody">
            
            <GroupContent
				mainTitle = "Send options"
				className = "SendPageContainer"
			>
				<AccountsCard 
					title = "Send to other user"
					to = "/send-other-user"
				/>
				<AccountsCard 
					title = "Send to external bank account"
					to = ""
				/>
				<AccountsCard 
					title = "Send to beacon"
					to = ""
				/>
                <AccountsCard 
					title = "Send to QR code"
					to = ""
				/>
                <AccountsCard 
					title = "Pay bills"
					to = ""
				/>
			</GroupContent>
        </main>
        <Footer />
    </>
);

export default Send;
