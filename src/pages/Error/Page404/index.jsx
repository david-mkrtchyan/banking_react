import React from "react";
import { Link } from "react-router-dom";

import Logo from "~c/includes/Logo";
import "./styled.scss";

const Page404 = () => (
    <section className = "Error404">
        <Logo />
        <h1> Coming soon... </h1>
        <Link to = "/" className = "backLinkError">
            Back 
        </Link>
    </section>
);

export default Page404;
