import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { SendAnother } from "Action";
import { Redirect, Link } from "react-router-dom";

//Components
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import ConfirmToSend from "~c/ConfirmToSend";
import Button from "UI/Button";
import { callPopUpMessage } from "UI/Messages";

//Engine
import { Decoding } from "Engine/Encryption";
import { ParseDecodingJSON, ParseJSON } from "Engine";
import { GetSession } from "BrowserStorage";

//Styles
import "./styled.scss";

const Confirm = ({
    isLoged,
    match,
    TransactionUserToUser,
    FormSendUser,
    ChangeAllFormAnother
}) => {
    const [ data, setData ] = useState(null);
    const [ choiceLink, setChoiceLink ] = useState(false);

    useEffect(() => {
        const CheckDataBLL = () => {
            const dataSU = ParseJSON(Decoding(GetSession("_su"), "_su"));
            ChangeAllFormAnother(dataSU)
            return dataSU;
        };

        if (!CheckDataBLL()) {
            let LinkData;
            try {
                LinkData = Decoding(match.params.data, "data");
            } catch (e) {
                window.location.replace("/send")
            };
            setData(ParseDecodingJSON(LinkData));
            setChoiceLink(true);
        }
    }, []);

    const handleClickConfirm = async () => {
        const message = await TransactionUserToUser(choiceLink ? data : FormSendUser )

        if (message.status >= 300) {
			(async () => {
                const feedBack = await message.json();
                Object.keys(feedBack).forEach(item => callPopUpMessage.error(feedBack[item][0]));
			})();
		};
    };

    return (
        <> 
            { !isLoged && <Redirect to = "/login" /> }
            <Header
                mainTitle = { match.path }
            />
            <main className = "Confirm Main scrollbar contentBody">
                <div className = "WrapperConfirm">
                    <ConfirmToSend 
                        Balance = {choiceLink ? (data && data.amount) : FormSendUser.amount}
                        Valute = {choiceLink ? (data && data.currency) : FormSendUser.currency}
                        PhoneNumber = {choiceLink ? (data && data.phone_number) : FormSendUser.phone_number}
                        AnotherUser = {choiceLink ? 
                            (data && `${data.first_name} ${data.last_name}`) 
                            : 
                            `${FormSendUser.first_name} ${FormSendUser.last_name}`
                        }
                        Message = {choiceLink ? (data && data.message) : FormSendUser.message}
                    />
                    <Button 
                        Margin = "15px 0 0 0" 
                        Padding = "17px 68px"
                        onClick = {handleClickConfirm}
                    >
                        <span className = "ConfirmButton"> CONFIRM </span>
                    </Button>
                    <Link to = "/top-up" className = "BackLink">
                        <span className = "NoBack">
                            NO,
                        </span>{" "}
                        go back and change.
                    </Link>
                </div>
            </main>
            <Footer />
        </>
    );
}

const mapState = state => ({
    isLoged: state.LogIn.isLoged,
    FormSendUser: state.SendAnotherProfil
});

const mapDispatch = dispatch => ({
    TransactionUserToUser: data => dispatch(SendAnother.TransactionUserToUser(data)),
    ChangeAllFormAnother: data => dispatch(SendAnother.ChangeAllFormAnother(data))
});

export default connect( mapState, mapDispatch )( Confirm );
