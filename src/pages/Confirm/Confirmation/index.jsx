import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { SendAnother } from "Action";

//Components
import Header from "Container/Includes/Header";
import Footer from "Container/Includes/Footer";
import ConfirmToSend from "~c/ConfirmToSend";

//IMG
import CheckSuccess from "IMG/CheckSuccess.png";
import CheckError from "IMG/CheckError.png";

//Engine
import { Decoding } from "Engine/Encryption";
import { ParseDecodingJSON, ParseJSON } from "Engine";
import { GetSession, RemoveSession } from "BrowserStorage";

//Styles
import "./styled.scss";

const Confirm = ({
    isLoged,
    match,
    FormSendUser,
    ChangeAllFormAnother
}) => {
    const [ data, setData ] = useState(null);
    const [ choiceLink, setChoiceLink ] = useState(false);

    useEffect(() => {
        const CheckDataBLL = () => {
            const dataSU = ParseJSON(Decoding(GetSession("_su"), "_su"));
            ChangeAllFormAnother(dataSU)
            return dataSU;
        };

        if (!CheckDataBLL()) {
            let LinkData;
            try {
                LinkData = Decoding(match.params.data, "data")
            } catch (e) {
                window.location.replace("/send")
            };
            setData(ParseDecodingJSON(LinkData));
            setChoiceLink(true);
        };
        return () => {
            RemoveSession("_su");
        };
    }, []);

    return (
        <> 
            { !isLoged && <Redirect to = "/login" /> }
            <Header
                mainTitle = { match.path }
            />
            <main className = "Confirmation Main scrollbar contentBody">
                <div className = "WrapperConfirmation">
                    <ConfirmToSend 
                        Balance = {choiceLink ? (data && data.amount) : FormSendUser.amount}
                        Valute = {choiceLink ? (data && data.currency) : FormSendUser.currency}
                        PhoneNumber = {choiceLink ? (data && data.phone_number) : FormSendUser.phone_number}
                        AnotherUser = {choiceLink ? 
                            (data && `${data.first_name} ${data.last_name}`) 
                            : 
                            `${FormSendUser.first_name} ${FormSendUser.last_name}`
                        }
                        Message = {choiceLink ? (data && data.message) : FormSendUser.message }
                        Mode = "confirm"
                    />
                    <div className = "imageBlock">
                        { ((choiceLink && (data && data.success)) || FormSendUser.success) ?
                            <img src = {CheckSuccess} alt = ""/> 
                            :
                            <img src = {CheckError} alt = ""/>
                        }
                    </div>
                </div>
            </main>
            <Footer />
        </>
    );
}

const mapState = state => ({
    isLoged: state.LogIn.isLoged,
    FormSendUser: state.SendAnotherProfil
});

const mapDispatch = dispatch => ({
    ChangeAllFormAnother: data => dispatch(SendAnother.ChangeAllFormAnother(data))
});


export default connect( mapState, mapDispatch )( Confirm );
