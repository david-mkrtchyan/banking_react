export const getJSON = async (url) => {
	const data = fetch(url)
		.then((response) => {
			if (!response.ok){
				throw new Error(response.statusText);
			}
			return response;
		});
	
	return data;
};

export const getIdJSON = async (url, id) => 
	fetch(`${url}/${id}`)
		.then((response) => {
			if (!response.ok){
				throw new Error(response.statusText);
			}
			return response;
		});
