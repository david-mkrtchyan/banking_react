export default (URL, header, body) => 
	fetch(URL, {
		"method": "POST",
		"mode": "cors",
		"cache": "no-cache",
		"credentials": "same-origin",
		"headers": {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${header}`
		},
		"redirect": "follow",
        "referrer": "no-referrer",
        "body": JSON.stringify(body && body)
	});