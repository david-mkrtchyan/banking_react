export const PutItemJSON = (url = "", id, data = {}) => 
	fetch(`${url}/${id}`, {
		method: "PUT",
		body: JSON.stringify(data),
		headers: {
			"Content-Type": "application/json"
		}
	});
