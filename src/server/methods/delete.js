export default (url, id) =>
	fetch(`${url}/${id || ""}`, { method: "DELETE" })
		.then((res) => {
			if (res.status !== 200) {
				throw new Error(res.status);
			}
			return res;
		});
