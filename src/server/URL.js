const API = "https://api20.wyrify.com/api/";

//Post
export const TokenPostURL = `${API}user/token-auth/`;
export const PostPinURL = `${API}user/check-pin/`;
export const RegisterURL = `${API}user/register/`;
export const CheckUserURL = `${API}user/token-auth/refresh/`;
export const UserCurrencyURL = `${API}user/user-currencies/`;
export const UserCheckingURL = `${API}user/check-user/`;
export const TransactionUserURL = `${API}user/transaction-send/`;
export const FeedBackURL = `${API}user/user-feed/`;
export const ExchangeDataUrl = `${API}user/exchange-data/`;
export const ExchangeURL = `${API}user/exchange/`;
