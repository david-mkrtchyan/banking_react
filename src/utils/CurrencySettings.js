export default (changeCurrency) => {
    return {
        dots: true,
        infinite: true,
        centerMode: true,
        speed: 500,
        slidesToShow: 1,
        centerPadding: "20%",
        slidesToScroll: 1,
        afterChange: (e) => changeCurrency(e),
    }
};
