import SettingsSlickFeed from "./SettingsSlickFeed";
import CurrencySettings from "./CurrencySettings";

export {
    SettingsSlickFeed,
    CurrencySettings
};