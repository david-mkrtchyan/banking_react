export default {
    dots: true,
    infinite: true,
    centerMode: true,
    speed: 500,
    slidesToShow: window.innerWidth <= 630 ? 1 : 2,
    centerPadding: '80px',
    slidesToScroll: window.innerWidth <= 630 ? 1 : 2,
    autoplay:true,
    autoplaySpeed:2000,
};
