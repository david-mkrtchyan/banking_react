import React from "react";

import "./styles.css";

const Loading = ( { children, loading } ) =>(
	<>
		{loading ? (
			<aside className = "Loading"> 
				<div className = "lds-ring">
					<div /><div /><div /><div />
				</div>
			</aside>
		) : (
			<>
				{children}
			</>
		)}
	</>
);

export default Loading;
