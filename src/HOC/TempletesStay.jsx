import React from "react";
import Footer from "~c/Footer";
import Header from "~c/Header";


const TempletesStay = ({ children }) => (
	<>
		<Header />
			{children}
		<Footer />
	</>
);

export default TempletesStay;
