import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { SendRefreshTokenUser, Login, Loading } from "Action";

//Engine
import { GetTokenRefresh, SetSession, GetLocal, GetSession } from "BrowserStorage";
import { Decoding } from "Engine/Encryption";
import { inactivityTime } from "Engine/Validation";

//Components
import { PopUpMessage, callPopUpMessage } from "UI/Messages";
import LoadingWait from "HOC/LoadingWait";

//Routes
import { routes, Authorization } from "./Routes";
import "GlobalStyle";

const Routes = ({ 
	location, 
	SendRefreshTokenUser, 
	LogSuccess,
	LogOutAction,
	LogOutWithoutToken,
	loading,
	LoadingStart,
	LoadingFailure
}) => {
	const [ inactive, setInactive ] = useState(false);
	const locationKey = location.pathname;

	//Routing Components
	const RoutesLayout = routes.map(item => <Route exact = {item.exact} path = {item.url} component = {item.component} key = {item.url} data-name = {item.name} />);
	const AuthorizationRoutes = Authorization.map(item => <Route exact = {item.exact} path = {item.url} component = {item.component} key = {item.url} data-name = {item.name} />);

	useEffect(() => {
		const token = GetTokenRefresh("_t");
		const user = Decoding(GetLocal("_u"), "_u");
		token && SendRefreshTokenUser(token);
		!token && user && LogOutAction(); 
		if (window.location.pathname !== "/verifedPhone") {
			sessionStorage.removeItem("_rs");
		};
		setTimeout(() => LoadingFailure(), 1500);

		return () => {
			LoadingStart();
		}
	}, [ locationKey ]);

	useEffect(() => {
		if (GetLocal("_t")){
			SetSession("_u", JSON.stringify({v_a: GetLocal("_t").verify_account, _l: true}));
			LogSuccess();
			inactivityTime(setInactive, LogOutWithoutToken);
			if (GetLocal("_dm")) {
				const bool = ( +new Date() - +GetLocal("_dm") ) >= 15778800000;
				bool && LogOutAction();
			};
		};
				
		if(GetSession("_vm")){
			callPopUpMessage.success(Decoding(GetSession("_vm"), "_vm"));
			sessionStorage.removeItem("_vm")
		};
		if(GetSession("_sm")){
			callPopUpMessage.success(Decoding(GetSession("_sm"), "_sm"));
			sessionStorage.removeItem("_sm")
		}
	}, []);

	return (
		<LoadingWait loading = {loading}>
			<main className = "wrapperHTML">
				<Switch>
					{RoutesLayout}
					{AuthorizationRoutes}
				</Switch>
				<PopUpMessage />
			</main>
		</LoadingWait>
	);
};

const mapState = state => ({
	loading: state.Loading.loading
});

const mapDispatch = dispatch => ({
	SendRefreshTokenUser: token => dispatch(SendRefreshTokenUser(token)),
	LogSuccess: () => dispatch(Login.Success()),
	LogOutAction: () => dispatch( Login.LogOutAction() ),
	LogOutWithoutToken: () => dispatch(Login.LogOutWithoutToken()),
	LoadingStart: () => dispatch( Loading.SetStartLoading() ),
	LoadingFailure: () => dispatch( Loading.SetExitLoading() )
});

export default connect( mapState, mapDispatch )( Routes );
