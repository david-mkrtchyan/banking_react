import HomePage from "Pages/Home";
import LogIn from "Pages/Login";
import Page404 from "Pages/Error/Page404";
import SignUp from "Pages/SignUp";
import VerifedPhone from "Pages/VerifedPhone";
import Currency from "Pages/Currency";
import Welcome from "Pages/Welcome";
import Send from "Pages/Send";
import SendOtherUser from "Pages/Send/SendOtherUser";
import Confirm from "Pages/Confirm";
import Confirmation from "Pages/Confirm/Confirmation";
import Exchange from "Pages/Exchange";
import ConfirmExchange from "Pages/Exchange/Confirm";
import ConfirmationExchange from "Pages/Exchange/Confirm/Confirmation";

export const routes = [
	{
		name: "home",
		url: "/",
		component: HomePage,
		exact: true
	},
	{
		name: "currency",
		url: "/currency",
		component: Currency,
		exact: true
	},
	{
		name: "send",
		url: "/send",
		component: Send,
		exact: true
	},
	{
		name: "sendOtherUser",
		url: "/send-other-user",
		component: SendOtherUser,
		exact: true
	},
	{
		name: "confirmSendOtherUser",
		url: "/confirm-send-other-user/:data",
		component: Confirm,
		exact: true
	},
	{
		name: "confirmationSendOtherUser",
		url: "/confirmation-send-other-user/:data",
		component: Confirmation,
		exact: true
	},
	{
		name: "exchange",
		url: "/exchange",
		component: Exchange,
		exact: true
	},
	{
		name: "confirmExchange",
		url: "/confirm-exchange/:data",
		component: ConfirmExchange,
		exact: true
	},
	{
		name: "confirmationExchange",
		url: "/confirmation-exchange/:data",
		component: ConfirmationExchange,
		exact: true
	}
];

export const Authorization = [
	{
		name: "logIn",
		url: "/logIn",
		component: LogIn,
		exact: false
	},
	{
		name: "signUp",
		url: "/signUp",
		component: SignUp,
		exact: false
	},
	{
		name: "verifedPhone",
		url: "/verifedPhone",
		component: VerifedPhone,
		exact: false
	},
	{
		name: "welcome",
		url: "/welcome",
		component: Welcome,
		exact: false
	},
	{
		url: "/**",
		component: Page404,
	}
];

const routesMap = {};

routes.forEach((routes) => {
	if (Object.keys(routes).some(item => item === "name")) {
		routesMap[routes.name] = routes.url;
	}
});

const urlBuilder = (name, params) => {
	if (!Object.keys(routesMap).some(item => item === name)) {
		return null;
	}
	let url = routesMap[name]; //Http/:id

	Object.keys(params).forEach(key => url = url.replace(`:${key}`, params[key]));

	return url;
};

export { routesMap, urlBuilder };
