export const Encription = (Incode, passCode) => {
	let textToChars = text => text.split('').map(c => c.charCodeAt(0))
    let byteHex = n => ("0" + Number(n).toString(16)).substr(-2)
    let applySaltToChar = code => textToChars(passCode).reduce((a,b) => a ^ b, code)    

    return Incode.split('')
        .map(textToChars)
        .map(applySaltToChar)
        .map(byteHex)
        .join('')
};

export const Decoding = (Incode, passCode) => {
	if(!Incode) return false;
	let textToChars = text => text.split('').map(c => c.charCodeAt(0))
    let saltChars = textToChars(passCode)
    let applySaltToChar = code => textToChars(passCode).reduce((a,b) => a ^ b, code)
    return Incode.match(/.{1,2}/g)
        .map(hex => parseInt(hex, 16))
        .map(applySaltToChar)
        .map(charCode => String.fromCharCode(charCode))
        .join('')
};
