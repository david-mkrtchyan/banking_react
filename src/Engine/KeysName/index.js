export const KeysName = keys => {
    switch(keys){
        case "country_code":
            return "Country";
        case "email":
            return null;
        case "first_name":
            return "First Name";
        case "last_name":
            return "Last Name";
        case "phone_number":
            return null;
        case "password":
            return "Password";
        case "password2":
            return "Confirm Password";
        default:
            return undefined;
    }
}

export const CheckKeysName = ( data ) => {
    const LengthData = Object.keys(data);
    let newData = [];

    LengthData.forEach(item => {
        const newKey = KeysName(item);
        if (newKey !== null) {
           newData.push( { name:[KeysName(item)], data: data[item] } );
        }
    });
    return false;
};
