//RemoveState
export const remove = (state, i) => {
  const products = state.products.filter((el, index) => index !== i);

  return {
    ...state,
    products
  };
};

//ChangeState
export const change = (state, i, current, items) => {
  const cloneItems = [...state[items]];

  cloneItems[i] = { ...cloneItems[i], current };
  return {
    ...state,
    cloneItems
  };
};

//HTML symbol in js
export const decodeEntities = s => {
  let temp = document.createElement("p");

  temp.innerHTML = s;
  const str = temp.textContent || temp.innerText;

  temp = null;
  return str;
};

//Index
export const index = (el) => {
  if (!el) return -1;
  var i = 0;
  do {
    i++;
  } while ((el = el.previousElementSibling));
  return i;
};

export const ParseDecodingJSON = string => {
  try {
    return JSON.parse(string);
  } catch (e) {
    console.log(e,string, "ParseDecodingJSON")
      let DecodingSplit = JSON.stringify(string).split("\\u0000");
      console.log(DecodingSplit, "DecodingSplit ParseDecodingJSON")
      try {
        return JSON.parse(JSON.parse(DecodingSplit.join("")));
      } catch(e) {
        // window.location.replace("/send")
      }
  }
}

export const ParseJSON = string => {
  try{
    return JSON.parse(string)
  } catch (e) {
    console.log(e, 'eParseJSOB')
    try {
      return JSON.parse(JSON.parse(string))
    } catch (e) {
      return false;
    }
  }
}

export const DelSpaces = (str) => {
	str = str.replace(/\s/g, '')
	return str;
}