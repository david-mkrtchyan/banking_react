import { isEmail } from "validator";
import { parsePhoneNumberFromString } from "libphonenumber-js";

const phoneFieldValid = (value) => {
	const phoneNumber = parsePhoneNumberFromString(value);

	if (phoneNumber) {
		return phoneNumber.isValid(value);
	} if (value.length === 1) {
		return null;
	} 
	
	return false;
};
  
export const isValid = (name, value, data) => {
	switch (name) {
		case "email":
			if (isEmail(value)) {
				return { valid: true, message: "" };
			}
			
			return { valid: false, message: "Invalid Email" };

		case "password":
			if (value.length <= 254 && value.length >= 8) {
				if (data) {
					if (!data.password2.value.length) {
						return { valid: true, message: "" };
					} if (
						data.password2.value.length &&
						value !== data.password2.value
					) {
						data.password2.valid = false;
						return { valid: false, message: "Passwords must match" };
					} 
					if (data && data.password2.value === value) {
						data.password2.valid = true;
					}
					return { valid: true, message: "" };
				}
				if (data && data.password2.value === value) {
					data.password2.valid = true;
				}
		
				return { valid: true, message: "" };
			} 
			return {
				valid: false,
				message: "Password must contain more than 8 digits"
			};
			
	  	case "password2":
		  if (
				value.length <= 254 &&
				value === data.password.value &&
				value.length >= 8
			) {
				if (data.password.value === value) {
					data.password.valid = true;
				}
				return { valid: true, message: "" };
			}
			return { valid: false, message: "Confirm Password must match the pin" };
			
		case "first_name":
			if (value.length >= 2 && value.search(/^[A-Za-z]+$/) !== -1) {
				return { valid: true, message: "" };
			} 
			return { valid: false, message: "The name you entered is incorrect" };
			
		case "last_name":
			if (value.length >= 2 && value.search(/^[A-Za-z]+$/) !== -1) {
				return { valid: true, message: "" };
			} 
			return { valid: false, message: "The name you entered is incorrect" };
			
		case "phone_number":
			if (phoneFieldValid(value)) {
				return { valid: true, message: "" };
			} 
			return { valid: false, message: "The number you entered is incorrect" };
			
		case "country_code":
			if (value.length) {
				return { valid: true, message: "" };
			} 
			return { valid: false, message: "Choose a country" };
			
		case "phone_pin":
			if (value.search(/^-?\d+\.?\d*$/) !== -1 && value.length === 4) {
				if (data) {
					if (!data.phone_pin.value.length) {
						return { valid: true, message: "" };
					} 
					return { valid: true, message: "" };
				};
				return { valid: true, message: "" };
			} 
			return { valid: false, message: "pin must contain 4 digits" };

		case "pin":
			if (value.search(/^-?\d+\.?\d*$/) !== -1 && value.length === 4) {
				if (data) {
					if (!data.pin2.value.length) {
						return { valid: true, message: "" };
					} if (data.pin2.value.length && value !== data.pin2.value) {
						data.pin2.valid = false;
						return { valid: false, message: "Passwords must match" };
					} 
					if (data && data.pin2.value === value) {
						data.pin2.valid = true;
					}
					return { valid: true, message: "" };
				}
				if (data && data.password2.value === value) {
					data.password2.valid = true;
				}
				return { valid: true, message: "" };
			} 
			if (data && data.pin2.value !== value){
				data.pin2.valid = false;
			}
			return { valid: false, message: "pin must contain 4 digits" };
			
		case "pin2":
			if (data) {
				if (
					value.search(/^-?\d+\.?\d*$/) !== -1 &&
			value.length === 4 &&
			value === data.pin.value
				) {
			
					if (!data.pin2.value.length) {
						return { valid: true, message: "" };
					} 
					if (data.pin.value === value) {
						data.pin.valid = true;
					}
					return { valid: true, message: "" };
				} 
				return { valid: false, message: "Confirm Password must match the pin" };
			};
			return { valid: true, message: "" };
		default:
			break;
	}
};
