export const notAllowed = (name, value) => {
	switch (name) {
		case "password":
			return value.length <= 254;
		case "password2":
			return value.length <= 254;
		case "phone_pin":
			return value.search(/^-?\d*\.?\d*$/) !== -1 && value.length <= 4;
		case "pin":
			return value.search(/^-?\d*\.?\d*$/) !== -1 && value.length <= 4;
		case "pin2":
			return value.search(/^-?\d*\.?\d*$/) !== -1 && value.length <= 4;
		default:
			return true;
	};
};
