import { SetLocal, GetLocal } from "BrowserStorage";

export default (setInactive, LogOutWithoutToken) => {
    let time;
    let ClearTimer;


    let events = ['mousedown', 'mousemove', 'keypress', 'scroll', 'touchstart'];
    events.forEach(e =>  {
        document.addEventListener(e, resetTimer, true);
    });


    function resetTimer () {
      SetLocal("_la", +new Date());
      clearTimeout( ClearTimer );
      clearTimeout( time );
      setInactive( false );
      time = setTimeout( fn , 1000 );
      ClearTimer = setTimeout( logoutByInactivity , 600000 );
    }

    checkLastActivity();
    resetTimer();

    function fn() {
      time++
    }

    function checkLastActivity() {
        if(GetLocal('_la')){
            let lastActiveTime = GetLocal('_la');
            let currentTime = +new Date();
            let inactivityTime = currentTime - lastActiveTime;
            let inactivityTimeInMinutes = Math.round(inactivityTime / 60000);
            if(inactivityTimeInMinutes > 10){
                logoutByInactivity()
            }
        }
    }

    function logoutByInactivity () {
        setInactive(true);
        LogOutWithoutToken();
    }

};
