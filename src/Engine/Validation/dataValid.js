export const dataValid = (data) => {
	const cloneData = { ...data };

	delete cloneData.isValidMessage;
	delete cloneData.phone_number;
	for (const key in cloneData) {
		if (cloneData[key].valid !== true) {
			return false;
		}
	};
	return true;
};

export const dataValidLogin = (data) => {
	const cloneData = { ...data };

	delete cloneData.isValidMessage;
	
	for (const key in cloneData) {
		if (cloneData[key].valid !== true) {
			return false;
		}
	};
	return true;
}
