import {getData, getDataPin, getDataPinLogin} from "./getData";
import {dataValid, dataValidLogin} from "./dataValid";
import {isValid} from "./isValid";
import {notAllowed} from "./notAllowed";
import inactivityTime from "./inactivityTime";

export {
	getData,
	getDataPin,
	getDataPinLogin,
	dataValid,
	isValid,
	notAllowed,
	dataValidLogin,
	inactivityTime
};
