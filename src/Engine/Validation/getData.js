export const getData = (data) => {
	const cloneData = {...data};

	let newData = {};

	for (const key in cloneData) {
		newData = {
			...newData,
			[key]: cloneData[key].value
		};
	};
	
	delete newData.isValidMessage;
	return newData;
};

export const getDataPin = (data) => {
	const cloneData = {...data};

	let newData = {};

	for (const key in cloneData) {
		newData = {
			...newData,
			[key]: cloneData[key].value
		};
	};
	const PhoneNumber = localStorage.getItem("_pn");
	const newPhone = PhoneNumber.substring(0, PhoneNumber.length);
	newData.phone_number = newPhone
	

	delete newData.isValidMessage;
	return newData;
};

export const getDataPinLogin = (data) => {
	const newData = {
		"password": data
	}
	const PhoneNumber = localStorage.getItem("_pn");
	const newPhone = PhoneNumber.substring(0, PhoneNumber.length);
	newData.phone_number = newPhone

	return newData;
};
