import { Decoding } from "Engine/Encryption"

//LocalStorage

export const SetLocal = (name, body) => {
	if(typeof body === "object"){
		localStorage.setItem(name, JSON.stringify(body));
	}else{
		localStorage.setItem(name, body);
	}
};

export const GetLocal = (name) => {
	if (localStorage.getItem(name)){
		let Local;
		try {
			Local = JSON.parse(localStorage.getItem(name));
		} catch (e) {
			Local = localStorage.getItem(name)
		}
		return Local;
	};
	return null;
};

export const GetToken = () => {
	if (localStorage.getItem("_t")) {
		const TokenLocal = localStorage.getItem("_t");
		const DecodingUser = Decoding(TokenLocal, "_t");
		try {
			return JSON.parse(`${DecodingUser}`);
		} catch ( error ) {
			let DecodingUserSplit = JSON.stringify(DecodingUser).split("\\u0000");
			DecodingUserSplit = JSON.parse(DecodingUserSplit.join("")).split(`"`);
			return {
				"refresh": DecodingUserSplit[3],
				"access": DecodingUserSplit[7],
				"verify_account": ~DecodingUserSplit[10].indexOf("true") ? true : false
			};
		};
	};
	return null;
};

export const GetTokenRefresh = () => {
	if ( localStorage.getItem("_t") ) {
		try {
			const RefreshToken = GetToken().refresh;
			return RefreshToken;
		} catch (error) {
			return false
		};
	};
	return null
};

export const GetTokenAccess = () => {
	if (localStorage.getItem("_t")) {
		try {
			const AccessToken = GetToken().access;
			return AccessToken;
		}catch(error){
			return false
		};
	};
	return null
};


export const RemoveLocal = (name) => {
	if (GetLocal(name)){
		localStorage.removeItem(name)
		return true;
	}
	return false;
}

export const ClearLocal = () => localStorage.clear();

//SessionStorage

export const SetSession = (name, body) => {
	sessionStorage.setItem(name, body);
};

export const GetSession = (name) => {
	if (sessionStorage.getItem(name)){
		return sessionStorage.getItem(name);
	}
	return null;
};

export const RemoveSession = (name) => {
	if (GetSession(name)){
		sessionStorage.removeItem(name)
		return true;
	}
	return false;
}

export const ClearSession = () => sessionStorage.clear();

//Cookie

export const getCookie = (name) => {
	const matches = document.cookie.match(new RegExp(
		`(?:^|; )${  name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1")  }=([^;]*)`
	));

	return matches ? decodeURIComponent(matches[1]) : undefined;
};

export const setCookie = (name, value) => {
	const setValue = `${name}=${JSON.stringify(value)}`;

	document.cookie = setValue;
};
