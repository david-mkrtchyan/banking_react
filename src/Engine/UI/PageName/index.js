export default path => {
    switch (path) {
        case "/":
            return "Home";
        case "/logIn":
            return "Login";
        case "/signup":
            return "Sign Up";
        case "/verifedPhone":
            return "Verifed Phone";
        case "/currency":
            return "Currency";
        case "/send":
            return "Send";
        case "/confirm-send-other-user/:data":
            return "Confirm";
        case "/confirmation-send-other-user/:data":
            return "Confirmation";
        case "/exchange":
            return "Exchange";
        case "/confirm-exchange/:data":
            return "Confirm";
        case "/confirmation-exchange/:data":
            return "Confirmation";
        default:
            return "Wyrify";
    };
};
