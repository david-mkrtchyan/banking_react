import * as Input from "SVG_Input";

const ChooseInput = (valute) => {
    switch ( valute ) {
        case "Name":
            return Input.UserSVG;
        case "Email":
            return Input.EmailSVG;
        case "Password":
            return Input.LockSVG;
        case "PhonePin":
            return Input.VerifedPhoneSVG;
        case "Pin":
            return Input.PinSVG;
        default:
            return null;
    }
}

export default ChooseInput;
