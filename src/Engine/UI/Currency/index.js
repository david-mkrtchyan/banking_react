import * as Currency from "SVG_Currency";


const ChooseCurrency = (valute) => {
    switch ( valute ) {
        case "USD":
            return Currency.USD_SVG;
        case "IQD":
            return Currency.IQD_SVG;
        case "BTC":
            return Currency.BTC_SVG;
        case "GBP":
            return Currency.GBP_SVG;
        case "EUR":
            return Currency.EUR_SVG;
        default:
            return null;
    }
}

export default ChooseCurrency;
