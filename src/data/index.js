import { BoxList } from "./BoxList";
import { FeedBackData } from "./feedBackData";

export {
    BoxList,
    FeedBackData
};
