export const FeedBackData = [
    {
        "type": "CurrencyTransaction",
        "data": "17 Oct 2019",
        "money": "1.42",
        "valute": "USD",
        "user": "Joe Smith",
        "mode": "to",
        get id() {
            return 44
        }
    },
    {
        "type": "messageFeed",
        "data": "12 Oct 2019",
        "message": "New feature in Your App: We now support 4 extra currencies!",
        get id() {
            return 2
        }
    },
    {
        "type": "CurrencyTransaction",
        "data": "16 Oct 2019",
        "money": "1.42",
        "valute": "EUR",
        "user": "Joe Smith",
        "mode": "from",
        get id() {
            return 3
        }
    },
    {
        "type": "messageFeed",
        "data": "12 Oct 2019",
        "message": "New feature in Your App: We now support 4 extra currencies!",
        get id() {
            return 4
        }
    },
    {
        "type": "CurrencyTransaction",
        "data": "15 Oct 2019",
        "money": "1.42",
        "valute": "EUR",
        "user": "Joe Smith",
        "mode": "from",
        get id() {
            return 5
        }
    },
    {
        "type": "messageFeed",
        "data": "12 Oct 2019",
        "message": "New feature in Your App: We now support 4 extra currencies!",
        get id() {
            return 6
        }
    },
    {
        "type": "CurrencyTransaction",
        "data": "14 Oct 2019",
        "money": "1.42",
        "valute": "EUR",
        "user": "Joe Smith",
        "mode": "from",
        get id() {
            return 7
        }
    },
    {
        "type": "messageFeed",
        "data": "12 Oct 2019",
        "message": "New feature in Your App: We now support 4 extra currencies!",
        get id() {
            return 8 
        }
    },
]