import React from "react";
import { MessageSVG } from "SVG"; 

import "./styled.scss";

const Message = () => (
	<MessageSVG 
        size = "22px"
        name = "MessageSVG"
    />
);

export default Message;
