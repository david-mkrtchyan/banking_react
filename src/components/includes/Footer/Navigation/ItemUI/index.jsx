import React from "react";

import "./styled.scss";

const ItemUI = ({
    SVG,
    content
}) => (
    <div className = "NavigationItem" >
        <div>
            {content === "Exchange" ? 
                <div className = "wrapperSVG"> 
                     <SVG
                        size = "23px"
                        name = "NavigationItemSVG"
                    />
                </div> 
            :
                <SVG
                    size = "23px"
                    name = "NavigationItemSVG"
                />
            }
           
        </div>
        <p className = "NavigationContent"> 
            {content}
        </p>
    </div>
);

export default ItemUI;
