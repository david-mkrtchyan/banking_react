import React from "react";
import { NavLink } from "react-router-dom";
import { HomeSVG, SendSVG, PlusSVG, ExchangeSVG, MoreSVG } from "SVG";
import ItemUI from "./ItemUI";

import "./styles.scss";

const Navigation = () => (
	<div className = "Navigation">
		<ul>
			<li>
				<NavLink 
					exact
					to = {{
						pathname: "/topup",
					}}
				>
					<ItemUI 
						SVG = {PlusSVG}
						content = "Top Up"
					/>
				</NavLink>
			</li>
			<li>
				<NavLink 
					exact
					to = {{
						pathname: "/send",
					}}
				>
					<ItemUI 
						SVG = {SendSVG}
						content = "Send"
					/>
				</NavLink>
			</li>
			<li>
				<NavLink exact to = "/" className = "NavigationItem HomeItem">
					<HomeSVG
						size = "40px"
						name = "HomeSVG"
					/>
				</NavLink>
			</li>
			<li>
				<NavLink 
					exact
					to = {{
						pathname: "/exchange",
					}}
				>
					<ItemUI 
						SVG = {ExchangeSVG}
						content = "Exchange"
					/>
				</NavLink>
			</li>
			<li>
				<NavLink 
					exact
					to = {{
						pathname: "/more",
					}}
				>
					<ItemUI 
						SVG = {MoreSVG}
						content = "More"
					/>
				</NavLink>
			</li>
		</ul>
	</div>
);

export default Navigation;
