import React from "react";
import propTypes from "prop-types";
import LogoImage from "IMG/Logo.png";

import "./style.scss";

const Logo = ({ width }) => (
	<h1 className = "Logo"> 
		<img 
			src = {LogoImage} 
			alt = "LogoImage" 
			className = "LogoImage" 
			style = {{width}}
		/>
	</h1>
);

Logo.propTypes = {
	width: propTypes.string
};

export default Logo;
