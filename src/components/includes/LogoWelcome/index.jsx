// import React from "react";
// import { Link } from "react-router-dom";
// import Logo from "IMG/Logo.png";



// const LogoWelcome = () => (
//   <div id = "page-transitions">
//     <div className = "page-hider"></div>
//     <div className = "page-content page-content-full">
//       <div className = "coverpage coverpage-full walkthrough-buttons">
//         <div className = "cover-walkthrough-slider ">
//           <div className = "cover-item">
//             <div className = "cover-content cover-content-center">
//               <div className = "avatar">
//                 <img src={Logo} alt = "Skytsunami" />
//                 <i></i>
//               </div>
//               <Link
//                 to = "/logIn"
//                 className = "top-40  uppercase welcome-start"
//               >
//                 Startup Now
//               </Link>
//             </div>
//             <div className = "cover-overlay overlay bg-theme">
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   </div>
// );

// export default LogoWelcome;

import React from "react";
import { Link } from "react-router-dom";
import Logo from "IMG/Logo.png";
import "Style/Libs/framework.css";
import "Style/Libs/style.css";

import "./styled.scss";

const LogoWelcome = () => (
	<div id = "page-transitions">
		<div >
			<div className = "avatar">
				<img src={Logo} alt = "Skytsunami" className = "avatarImage" />
			</div>
			<Link
				to = "/logIn"
				className = "top-40  uppercase welcome-start"
			>
				Startup Now
			</Link>
		</div>
	</div>
);

export default LogoWelcome;
