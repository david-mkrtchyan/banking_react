import React from "react";
import "./styled.scss";

const SendToCurrency = ({
    onChange,
    value,
    currency
}) => (
    <section className = "SendToCurrency">
        <input 
            type = "text"
            name = "amount"
            placeholder = "amount"
            value = {value}
            className = "amount-input"
            onChange = { onChange }
        />
        <span className = "SendToCurrencyText" > { currency } </span>
    </section>
);

export default SendToCurrency;
