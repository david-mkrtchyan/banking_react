import React from "react";

//Styles
import {Container} from "./StyledConfirm.js";

const Confirm = ({
									 ConfirmTitle,
									 ConfirmToText,
									 Amount,
									 BaseCurrency,
									 SwapCurrency,
									 Price
								 }) => (
	<Container>
		<h2 className="ConfirmTitle">{ConfirmTitle}</h2>
		<div className="CurrencyExchange bottom-50">
			<span className="Balance"> {Amount} </span>
			<span className="Valute"> {BaseCurrency} </span>
		</div>
		<div className="WrapperNavigationConfirm">
			<div className="TitleConfirm ToConfirm">
				<p className="ToTitle color-theme center-text opacity-100 bottom-30">{ConfirmToText}</p>
				<span className="Balance"> {Price} </span>
				<span className="Valute"> {SwapCurrency} </span>
			</div>
		</div>
	</Container>
);

export default Confirm;
