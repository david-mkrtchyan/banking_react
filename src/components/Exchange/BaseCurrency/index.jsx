import React from "react";
import "./styled.scss";

const BaseCurrency = ({
    onChange,
    value,
    name,
    currency
}) => (
    <section className = "SendToCurrency">
        <input 
            type = "text"
            name = {name}
            placeholder = {name}
            value = {value}
            className = "amount-input"
            onChange = { onChange }
        />
        <span className = "SendToCurrencyText" > { currency } </span>
    </section>
);

export default BaseCurrency;
