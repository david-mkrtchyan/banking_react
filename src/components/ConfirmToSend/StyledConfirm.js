import styled from "styled-components";

export const Container = styled.div`
    margin-top: 45px;
    .ConfirmTitle{
        font-size: 18px;
        color: #939393;
        font-weight: 500;
        margin-bottom: 20px;
    };
    .CurrencySend{
        margin-bottom: ${props => props.Mode === "send" ? "50px" : "60px" };
        color: white;
        .Balance{
            font-weight: 400;
            font-size: ${props => props.Mode === "send" ? "46px" : "59px" } ;
        }
        .Valute{
            font-weight: 450;
            font-size: ${props => props.Mode === "send" ? "22px" : "26px" } ;
        }
    }
    .WrapperNavigationConfirm{
        margin-bottom: 50px;
        .TitleConfirm{
            margin-bottom:  ${props => props.Mode === "send" ? "32px" : "38px" };
            .ToTitle{
                color: #b1b1b1;
                font-size:  ${props => props.Mode === "send" ? "22px" : "26px" };
                margin-bottom: 2px;
            }
            .TextConfirm{
                font-size:  ${props => props.Mode === "send" ? "18px" : "22px" };
                color: white; 
                line-height: ${props => props.Mode === "send" ? "28px" : "35px" };
            }
        }
    }
`;
