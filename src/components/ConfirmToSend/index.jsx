import React from "react";

//Styles
import { Container } from "./StyledConfirm.js";

const ConfirmToSend = ({
    Balance,
    Valute,
    PhoneNumber,
    AnotherUser,
    Message,
    Mode = "send"
}) => (
    <Container Mode = {Mode}>
        { Mode == "send" ? <h2 className = "ConfirmTitle"> Please confirm sending :</h2> : null }
        <div className = "CurrencySend">
            <span className = "Balance"> {Balance} </span>
            <span className = "Valute"> {Valute} </span>
        </div>
        <div className = "WrapperNavigationConfirm">
            <div className = "TitleConfirm ToConfirm" >
                <p className = "ToTitle" > To: </p>
                <p className = "TextConfirm ToPhoneNumber" > { PhoneNumber } </p>
                <p className = "TextConfirm ToAnotherUser" > { AnotherUser } </p>
            </div>
            <div className = "TitleConfirm">
                <p className = "ToTitle" > Message: </p>
                <p className = "TextConfirm ToMessage" > { Message } </p>
           </div>
        </div>
    </Container>
);

export default ConfirmToSend;
