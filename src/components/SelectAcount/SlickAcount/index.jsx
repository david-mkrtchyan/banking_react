import React from "react";
import Slider from "react-slick";
import { CurrencySettings } from "UTILS";

const SlickAcount = ({
    Currency,
    changeCurrency
}) => {
  console.log("CurrencyCurrency",Currency);
    const mapCurrency = () => Currency.map(item => (
        <div  className = "CurrencyMaping">
            <span> {item.name} </span>
            { window.innerWidth <= 450 ? <span> {item.balance}.00 </span> : null } 
        </div>
    ));

    return (
        <>
            <Slider {...CurrencySettings(changeCurrency)}>
                {mapCurrency()}
            </Slider>
        </>
    );
};

export default SlickAcount;
