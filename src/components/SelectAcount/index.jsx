import React from "react";
import SlickAcount from "./SlickAcount";

import "./styled.scss";

const SelectAcount = ({
    Currency,
    changeCurrency
}) => (
        <section className = "SelectAccountSection">
            <h1 className = "MainTitleSelect"> Select account </h1>
            <SlickAcount 
                Currency = {Currency}
                changeCurrency = {changeCurrency}
            />
        </section>
);

export default SelectAcount;
