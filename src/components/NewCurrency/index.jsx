import React from "react";
import { Link } from "react-router-dom";

import "./styled.css";

const NewCurrency = () => (
    <p className = "NewCurrency"> 
        <Link to = "/" className = "addCurrency">
            <span className = "PlusCreate"> + </span> create new 
        </Link>
    </p>
);

export default NewCurrency;
