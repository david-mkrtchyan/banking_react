import React from "react";
import propTypes from "prop-types";
import { Link } from "react-router-dom";
import Input from "UI/Input";
import Button from "UI/Button";
import PhoneField from "UI/PhoneField";

import "./styled.scss";

const LogIn = ({
	btnDisabled,
	HandleSubmitLog,
	FormLogin,
	handlePhoneChange,
	handleFormChange
}) =>  (
	<form 
		onSubmit = {HandleSubmitLog}
		className = "FormLogin"
	>
		<PhoneField
			label = "Phone Number"
			value = {FormLogin.phone_number.value}
			valid = {FormLogin.phone_number.valid}
			onChange = {handlePhoneChange}
		/>
		<Input
			name = "password"
			label = "Password"
			type = "password"
			placeholder = "Password"
			value = {FormLogin.password.value}
			valid = {FormLogin.password.valid}
			onChange = {handleFormChange}
			inpWidth = {70}
		/> 
		<Button 
			disabled = {btnDisabled}
			className = "FormButton"
		>
			Login
		</Button>
		<Link 
			to = "/signup"
			className = "FormLoginLink"
		>
			Sign Up
		</Link>
	</form>
);

LogIn.propTypes = {
	btnDisabled: propTypes.bool,
	FormLogin: propTypes.object,
	handleFormChange: propTypes.func,
	handlePhoneChange: propTypes.func
};

export default LogIn;
