import React from "react";
import propTypes from "prop-types";
import { Link } from "react-router-dom";
import Input from "UI/Input";
import Button from "UI/Button";

import "./styled.scss";

const VerifedPhone = ({
    btnDisabled,
    FormPin,
    handleFormChange,
    HandleSubmitPin
}) => (
    <form 
        onSubmit = {HandleSubmitPin}
        className = "FormVerifed"
    >
        <Input
            name = "phone_pin"
            type = 'text'
            label = "PhonePin"
            placeholder = "&nbsp;&#8226;&nbsp;&#8226;&nbsp;&#8226;&nbsp;&#8226;"
            value = {FormPin.phone_pin.value}
            valid = {FormPin.phone_pin.valid}
            onChange = {handleFormChange}
        />
        <Input
            name = "pin"
            type = 'text'
            label = "Pin"
            placeholder = "&nbsp;&#8226;&nbsp;&#8226;&nbsp;&#8226;&nbsp;&#8226;"
            value = {FormPin.pin.value}
            valid = {FormPin.pin.valid}
            onChange = {handleFormChange}
        />
        <Input
            name = "pin2"
            type = 'text'
            label = "Pin"
            placeholder = "&nbsp;&#8226;&nbsp;&#8226;&nbsp;&#8226;&nbsp;&#8226;"
            value = {FormPin.pin2.value}
            valid = {FormPin.pin2.valid}
            onChange = {handleFormChange}
        />
        <Button 
            disabled = {btnDisabled}
            className = "FormButton"
        >
			Login
		</Button>
        {/* <Link 
			to = "/login"
			className = "FormLoginLink"
		>
			Login
		</Link> */}
    </form>
);

VerifedPhone.propTypes = {
    btnDisabled: propTypes.bool,
	FormVerifed: propTypes.object,
    handleFormChange: propTypes.func,
    HandleSubmitPin: propTypes.func
};

export default VerifedPhone;
