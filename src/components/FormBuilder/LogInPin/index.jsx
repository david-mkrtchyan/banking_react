import React, { useEffect } from "react";
import propTypes from "prop-types";
import { Link } from "react-router-dom";
import { index } from "Engine";

import "./styled.scss";

const LogInPin = ({
	HandleSubmitLog,
    FeedBack,
    setFeedBack
}) =>  {
    let input 	= '';
    const numbers = [1,2,3,4,5,6,7,8,9,0];
    const mapNumbersPhone = () => numbers.map(item => (
        <div 
            className = {`number`}
            key = {item}
        >{item}</div>
    ));

    const HandleClickNumber = function (e, dots) {
        let number = e.target;
        
        number.classList.add( 'grow' );
        input += ( index(number) );
        //
        const DotLength = document.querySelectorAll(".dot");
        const ActiveDot = [];
        for(let i = 0; i < DotLength.length; i++){
            DotLength[i].classList.forEach(item => item === "active" ? ActiveDot.push(true) : null)
        }
        if(input.length <= 4){
            dots[input.length-1].classList.add('active')
        }
        //
        if( input.length >= 4 ) {
            HandleSubmitLog(input)
            setTimeout(function() {
                dots.forEach( (dot) => dot.className = 'dot' );
                input = '';
                setFeedBack(true);
            }, 900);
        };
       
  
        setTimeout(function() {
                number.className = 'number';
        }, 1000);
    }
    useEffect(() => {
        let dots    =  Array.from(document.getElementsByClassName('dot'));
        let numbersBox = document.querySelectorAll('.number');

        numbersBox.forEach(item => item.addEventListener("click", (e) => HandleClickNumber(e, dots)));
    }, []);

    return (
        <>
            <div id = "pin">
                <div className = "dots">
                    <div className = {`dot ${FeedBack ? "" : "wrong"}`} />
                    <div className = {`dot ${FeedBack ? "" : "wrong"}`} />
                    <div className = {`dot ${FeedBack ? "" : "wrong"}`} />
                    <div className = {`dot ${FeedBack ? "" : "wrong"}`} />
                </div>
                <p> 
                    <Link to = "#" className = "FormLoginLink">
                        Forgot ?    
                    </Link> 
                    &nbsp;|&nbsp;
                    <Link to = "/signup" className = "FormLoginLink">
                        Sign Up
                    </Link>
                </p>
                <div className = "numbers">
                    { mapNumbersPhone() }
                </div>
            </div>
        </>
    );
};

LogInPin.propTypes = {
    handleFormChangePin: propTypes.func,
    HandleSubmitLog: propTypes.func
};

export default LogInPin;