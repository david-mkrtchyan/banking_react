import React from "react";
import { Link } from "react-router-dom";
import propTypes from "prop-types";
import Input from "UI/Input";
import Button from "UI/Button";
import PhoneField from "UI/PhoneField";
import CountryField from "UI/CountryField";
import { UserSVG, EmailSVG, LockSVG } from "SVG";

import "./styled.scss";

const SignUp = ({
	btnDisabled,
	HandleSubmitSign,
	FormSignUp,
	handlePhoneCountryChange,
	handleFormChange
}) => (
	<form 
		onSubmit = {HandleSubmitSign}
		className = "FormSignUp"
	>
		<PhoneField
			label = "Phone Number"
			value = {FormSignUp.phone_number.value}
			valid = {FormSignUp.phone_number.valid}
			onChange = {handlePhoneCountryChange}
		/>
		<Input
			name = "email"
			label = "Email"
			placeholder = "Email Address"
			value = {FormSignUp.email.value}
			valid = {FormSignUp.email.valid}
			onChange = {handleFormChange}
		/>
		<Input
			name = "first_name"
			label = "Name"
			placeholder = "First Name"
			value = {FormSignUp.first_name.value}
			valid = {FormSignUp.first_name.valid}
			onChange = {handleFormChange}
		/>
		<Input
			name = "last_name"
			label = "Name"
			placeholder = "Last Name"
			value = {FormSignUp.last_name.value}
			valid = {FormSignUp.last_name.valid}
			onChange = {handleFormChange}
		/>
		<CountryField
			label = "Country"
			valid = {FormSignUp.country_code.valid}
			onChange = {handlePhoneCountryChange}
			country = {FormSignUp.country_code.value}
		/>
		<Input
			name = "password"
			type = "password"
			label = "Password"
			placeholder = "Password"
			value = {FormSignUp.password.value}
			valid = {FormSignUp.password.valid}
			onChange = {handleFormChange}
		/>
		<Input
			name = "password2"
			type = "password"
			label = "Password"
			placeholder = "Confirm Password"
			value = {FormSignUp.password2.value}
			valid = {FormSignUp.password2.valid}
			onChange = {handleFormChange}
		/>
		<Button 
			disabled = {btnDisabled}
			className = "FormButton"
		>
			Sign Up
		</Button>
		<Link 
			to = "/login"
			className = "FormLoginLink"
		>
			Login
		</Link>
	</form>
);

SignUp.propTypes = {
	btnDisabled: propTypes.bool,
	FormSignUp: propTypes.object,
	handleFormChange: propTypes.func,
	handlePhoneCountryChange: propTypes.func,
	HandleSubmitSign: propTypes.func
};

export default SignUp;
