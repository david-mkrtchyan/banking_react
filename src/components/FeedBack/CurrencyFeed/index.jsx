import React from "react";
import ChooseCurrency from "Engine/UI/Currency";

import "./styled.scss";

const CurrencyFeed = ({
    timestamp,
    amount,
    currency,
    user_name,
}) => { 
    let CurrencySVG = ChooseCurrency(currency);

    return (
        <div className = "team-item CurrencyFeed">
            <p className = "center-left">
                <p> {timestamp} : </p>
                {`${user_name}`}
            </p>
            <p className = "p-slider">
                <CurrencySVG
                    size = "15px"
                    name = "CurrencySVG"
                /> 
                {amount}.<span>00 </span> </p>
        </div>
    );
}

export default CurrencyFeed;
