import React from "react";

import "./styled.scss";

const MessageFeed = ({ data, message }) => (
    <aside className = "team-item messageFeed">
        <p className = "p-dat">{data} :</p>
        <p className = "center-left p-discription">
            {message}
        </p>
    </aside>
);

export default MessageFeed;
