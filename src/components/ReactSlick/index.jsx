import React from "react";
import Slider from "react-slick";

import CurrencyFeed from "~c/FeedBack/CurrencyFeed";
import MessageFeed from "~c/FeedBack/MessageFeed";
import { SettingsSlickFeed } from "UTILS";

import "Libs/Slick-carousel/slick/slick.css";
import "Libs/Slick-carousel/slick/slick-theme.css";

class Parent extends React.Component {
    state = {
        selectedFooter: 1
    }
    
    render() {
        return (
            <div>
                <SimpleSlider 
                    settings = {SettingsSlickFeed} 
                    FeedBackMessages = {this.props.FeedBackMessages} 
                />
            </div> 
        );
    }
}
  
const SimpleSlider = ({FeedBackMessages, settings}) => {
    const mapFeedBack = () => FeedBackMessages.map(item => (
        <div>
            {item.currency === null ?
                <MessageFeed {...item} />
                :
                <CurrencyFeed {...item} />
            }
        </div>
    ));

    return (
        <>
            {FeedBackMessages.length ? <div className = "MainTitleSlick"> Your feed </div> : null }
            <Slider {...settings}>
                {mapFeedBack()}
            </Slider>
        </>
    )
}
          
export default Parent;