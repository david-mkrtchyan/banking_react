import React from "react";
import ReactSlick from "~c/ReactSlick";

import "./styled.scss";

const FeedBack = ({FeedBackMessages}) => (
    <>
        <ReactSlick FeedBackMessages = {FeedBackMessages} />
    </>
);

export default FeedBack;
