import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import { SendAnother } from "Action";

import SendToCurrency from "~c/SendToCurrency";
import PhoneField from "UI/PhoneField";
import TextAreaMessage from "UI/TextAreaMessage";
import Button from "UI/Button";
import { callPopUpMessage } from "UI/Messages";

const CurrencySend = ({
	ChangeAnotherUserForm,
	FormSendAnother,
	valute,
	balance,
	CheckUserPhoneHas,
}) => {
	const [btnDisabled, setDisabled ] = useState(true)
	
	const handlePhoneChange = (name, value) => {
		ChangeAnotherUserForm(value, name);
	};

	useEffect(() => {
		(FormSendAnother.amount > 0 && FormSendAnother.amount <= balance) && FormSendAnother.message.length?
			setDisabled(false)
			:
			setDisabled(true);
	}, [FormSendAnother.amount, balance, FormSendAnother.message])

	const handleChangeInput = (e) => {
		const { name, value } = e.target;

		ChangeAnotherUserForm(value, name);
	};

	const handleClickNext = async () => {
		const message = await CheckUserPhoneHas(FormSendAnother);
		
		if (message.status >= 300) {
			(async () => {
				const feedBack = await message.json();
				callPopUpMessage.error(feedBack.message);
			})();
		};
	};

	return (
		<>
			<SendToCurrency 
				onChange = {handleChangeInput}
				value = {FormSendAnother.amount}
				currency = {valute}
			/>
			<PhoneField 
				onChange = {handlePhoneChange}
				value = {FormSendAnother.phone_number}
			/>
			<TextAreaMessage 
				onChange = {handleChangeInput}
				name = "message"
				value = {FormSendAnother.message}
			/>
			<Button 
				onClick = { handleClickNext }
				disabled = { btnDisabled }
			>
				<p className = "ButtonSendAnotherUser"> Next </p>
			</Button>
		</>
	);
};

const mapState = state => ({
	FormSendAnother: state.SendAnotherProfil
});

const mapDispatch = dispatch => ({
	ChangeAnotherUserForm: ( data, name ) => dispatch( SendAnother.ChangeAnotherUserForm( data, name ) ),
	CheckUserPhoneHas: FormSendAnother => dispatch(SendAnother.CheckUserPhoneHas(FormSendAnother))
});

export default connect( mapState, mapDispatch )( CurrencySend );
