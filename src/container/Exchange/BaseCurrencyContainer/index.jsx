import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import { ExchangeAction } from "Action";

import BaseCurrency from "~c/Exchange/BaseCurrency";
import { callPopUpMessage } from "UI/Messages";

const BaseCurrencyContainer = ({
	ChangeExchangeForm,
	ExchangeData,
	GetExchangeData,
	valute,
	balance,
	name
}) => {

	useEffect(() => {
		console.log(ExchangeData)
		if( (ExchangeData.amount > 0 && ExchangeData.amount <= balance) && ExchangeData.base_currency !== ExchangeData.swap_currency){
			console.log("============", ExchangeData.base_currency,ExchangeData.swap_currency );
			(async () => {
				const message = await GetExchangeData(ExchangeData);

				if (message.status >= 300) {
					(async () => {
						const feedBack = await message.json();
						callPopUpMessage.error(feedBack.message);
					})();
				}
			})();
		}else if(ExchangeData.base_currency === ExchangeData.swap_currency){
				setTimeout(()=>{
					ChangeExchangeForm( ExchangeData.amount, "price");
					ChangeExchangeForm( 1, "appExchange");
				},100)
		}
	}, [ExchangeData.amount, ExchangeData.base_currency, ExchangeData.swap_currency])

	const handleChangeInput = (e) => {
		const { name, value } = e.target;
		ChangeExchangeForm(value, name);
	};

	return (
		<>
			<BaseCurrency
				onChange = {handleChangeInput}
				value = {ExchangeData.amount}
				name = {name}
				currency = {valute}
			/>
		</>
	);
};

const mapState = state => ({
	ExchangeData: state.ExchangeData
});

const mapDispatch = dispatch => ({
	ChangeExchangeForm: ( data, name ) => dispatch( ExchangeAction.ChangeExchangeForm( data, name ) ),
	GetExchangeData: ExchangeData => dispatch(ExchangeAction.GetExchangeData(ExchangeData))
});

export default connect( mapState, mapDispatch )( BaseCurrencyContainer );
