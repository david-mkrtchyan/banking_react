import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import { ExchangeAction } from "Action";

import SwapCurrency from "~c/Exchange/SwapCurrency";
import Button from "UI/Button";
import { callPopUpMessage } from "UI/Messages";

const SwapCurrencyContainer = ({
	ChangeExchangeForm,
	GoToExchangeConfirm,
	ExchangeData,
	valute,
	balance,
	name
}) => {
	const [btnDisabled, setDisabled ] = useState(true)
	


	useEffect(() => {
		((ExchangeData.amount > 0 && ExchangeData.amount <= balance) && ExchangeData.base_currency !== ExchangeData.swap_currency) ?
			setDisabled(false)
			:
			setDisabled(true);
	}, [ExchangeData.amount, ExchangeData.base_currency , ExchangeData.swap_currency]);

	const handleClickNext = async () => {
		GoToExchangeConfirm(ExchangeData);
	};


	return (
		<>
			<SwapCurrency
				value = {ExchangeData[name]}
				name = {name}
				currency = {valute}
			/>
			<span className="exchange-rate">Exchange rate : {ExchangeData.appExchange}</span>
			<Button
				onClick = { handleClickNext }
				disabled = { btnDisabled }
			>
				<p className = "ButtonSendAnotherUser"> Next </p>
			</Button>
		</>
	);
};

const mapState = state => ({
	ExchangeData: state.ExchangeData
});

const mapDispatch = dispatch => ({
	ChangeExchangeForm: ( data, name ) => dispatch( ExchangeAction.ChangeExchangeForm( data, name ) ),
	GoToExchangeConfirm: ExchangeData => dispatch( ExchangeAction.GoToExchangeConfirm(ExchangeData) )
});

export default connect( mapState, mapDispatch )( SwapCurrencyContainer );
