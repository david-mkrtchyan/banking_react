import React, { useState } from 'react';
import { connect } from "react-redux";

import { Currency } from "Action";
import { isValid } from "Engine/Validation";
import { BoxList } from "Data";

//Components
import Accordion from "UI/Accordion";
import { callPopUpMessage } from "UI/Messages";
import Modal from "UI/ModalSend";

//Styles
import { Container, Content } from "./StyledBox";

const BoxCurrency = ( {
    ChangeDataCurrency,
    FormCurrency,
    currency, 
    balance, 
    id, 
    SendCurrencyAmount, 
    TransferCurrencyAmount, 
    CheckUserCurrency
} ) => {
    const [ modal, setModal ] = useState(false);
    const [ dataProfil, setDataProfil ] = useState({});
    const [ currencyCount, setCurrencyCount ] = useState("");
  
    const handlePhoneChange = (name, value) => {
        ChangeDataCurrency(isValid(name, value).valid, name, "valid");
		ChangeDataCurrency(value, name, "value");
		ChangeDataCurrency(isValid(name, value).message, "isValidMessage");
	};

	const selectModal = () => {
		setModal(!modal)
    };

	const handleSendAnotherUser = async (data) => {
		const dataSendUser = {
			phone_number: FormCurrency.phone_number.value,
			account_id: id,
			amount: currencyCount
		};
		const feedBack = await TransferCurrencyAmount(dataSendUser);

        if(feedBack.status >= 300){
            (async () => {
                const message = await feedBack.json();
                if(message.non_field_errors){
                    message.non_field_errors.forEach(item => callPopUpMessage.error(item))
                }
                if(message.message){
                    callPopUpMessage.error(message.message);
                }
                else{
                    callPopUpMessage.error(message.detail);
                }
            })();
            setModal(false)
            return;
		};
		(async () => {
			const message = await feedBack.json();
			
			callPopUpMessage.success(message.message);
			handleSendCurency();
			setModal(false);
		})();
	};

	const handleSendCurency = async () => {
		const message = await CheckUserCurrency()

		if (message.status >= 300) {
			(async () => {
				const feedBack = await message.json();
				callPopUpMessage.error(feedBack.detail);
			})();
		};
	};

    const handleCurrencyCount = (event) => {
        setCurrencyCount(event.target.value)
    }
  
    const handleSendCurrency = async () => {
        const data = {
            phone_number: FormCurrency.phone_number,
            account_id: id,
            currency: currencyCount
        }
        
        const feedBack = await SendCurrencyAmount(data);
        if(feedBack.status >= 300){
            (async () => {
                const message = await feedBack.json();
                
                if(message.message){
                    callPopUpMessage.error(message.message);
                }else{
                    callPopUpMessage.error(message.detail);
                }
            })();
            
            setModal(false)
            return;
        };
        (async () => {
            const message = await feedBack.json();
            setDataProfil(message.user);
            selectModal(message.user);
        })();
    }

    return (
        <>
            <Container>
                <Accordion
                    List = {BoxList}
                    currencyCount = {currencyCount}
                    handleCurrencyCount = {handleCurrencyCount}
                    formData = { FormCurrency }
                    handlePhoneChange = {handlePhoneChange}
                    handleSendCurrency = {handleSendCurrency}
                >
                    <Content 
                        flex = {2} 
                    >
                        {currency}
                    </Content>
                    <Content> {balance} </Content>
                </Accordion>
            </Container>
            <Modal 
                displayModal = {modal}
                closeModal = {selectModal}
                money = {currencyCount}
                amountName = {currency}
                user_name = {dataProfil.first_name}
                email = {dataProfil.email}
                handleSendAnotherUser = {handleSendAnotherUser}
            />
        </>
    );
};

const mapStateToProps = state => ({
    FormCurrency: state.FormCurrency
});

const mapDispatchToProps = dispatch => ({
	SendCurrencyAmount: data => dispatch( Currency.SendCurrencyAmount( data ) ),
	TransferCurrencyAmount: data => dispatch( Currency.TransferCurrencyAmount( data ) ),
    CheckUserCurrency: () => dispatch( Currency.CheckUserCurrency() ),
    ChangeDataCurrency: ( data, name, key ) => dispatch( Currency.ChangeDataCurrency( data, name, key ) )
});

export default connect( mapStateToProps , mapDispatchToProps )( BoxCurrency );
