import styled from "styled-components";

export const Container = styled.div `
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid rgba(0, 0, 0, 0.200);
  cursor: pointer;
  &:last-child {

  border: none;
  }
  &:hover {
    background-color: rgba(0, 0, 0, 0.200);
  }
`;

export const Content = styled.div `
  flex: ${props => props.flex || 1};
  text-align:center;
`;