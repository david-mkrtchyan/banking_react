import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import propTypes from "prop-types";
import { SetHeaderHeight } from "Action";

//Engine
import PageName from "Engine/UI/PageName";

import Logo from "~c/includes/Logo";    

import "./styles.scss";

const Header = ({ SetHeaderHeight, mainTitle } ) => {
	const headerRef = useRef(null);

	useEffect(() => {
		SetHeaderHeight(headerRef.current.offsetHeight);
	}, []);

	return (
		<section
			ref = {headerRef} 
			className = "Header"
		>
			<div className = "LogoBlock">
				<Link to = "/"> 
					<Logo width = "42px" />
				</Link>
				<p className = "LinkText" > {PageName(mainTitle)} </p>
			</div>
		</section>
	);
};

Header.propTypes = {
	SetHeaderHeight: propTypes.func
};

const mapDispatch = dispatch => ({
	SetHeaderHeight: data => dispatch(SetHeaderHeight(data)),
});

export default connect( null, mapDispatch )( Header );
