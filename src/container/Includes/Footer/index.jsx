import React from "react";

//Components
import Navigation from "~c/includes/Footer/Navigation";

//Style
import "./styled.scss";

const Footer = () => (
	<footer className = "footer"> 
		<Navigation />
	</footer>
);

export default Footer;
