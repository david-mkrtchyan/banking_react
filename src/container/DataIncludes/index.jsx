import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import { Currency, SendRefreshTokenUser, CheckUserFeedBack } from "Action";
import LoadingWait from "HOC/LoadingWait";
import { GetTokenRefresh } from "BrowserStorage";

const DataIncludes = ({
	CheckUserCurrency,
	CheckUserFeedBack,
	children,
	SendRefreshTokenUser
}) => {
	const [ loading, setLoading ] = useState(true);

    useEffect(() => {
        const sendProfilData = async () => {
			setLoading(true)
			const message = await CheckUserCurrency();
			const messageFeedBack = await CheckUserFeedBack();
			if (message.status >= 300 || messageFeedBack.status >= 300) {
				const token = GetTokenRefresh("_t");
				(async () => {
					await SendRefreshTokenUser(token);
					sendProfilData()
				})();
				return;
			}

			setLoading(false)
		};
		sendProfilData();
    }, []);
    
    return (
		<LoadingWait loading = {loading}>
			{children}
		</LoadingWait>
    );
};


const mapDispatch = dispatch => ({
	CheckUserCurrency: () => dispatch(Currency.CheckUserCurrency()),
	CheckUserFeedBack: () => dispatch(CheckUserFeedBack()),
	SendRefreshTokenUser: token => dispatch(SendRefreshTokenUser(token))
});

export default connect( null, mapDispatch )( DataIncludes );
