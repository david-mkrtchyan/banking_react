import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { ChangeInitialDataSign, SignUpAction, Loading } from "Action";
import { isValid, notAllowed, dataValidLogin, getData } from "Engine/Validation";
import { CheckKeysName } from "Engine/KeysName";

//Components
import SignUpUI from "~c/FormBuilder/SignUp"; 
import LoadingWait from "HOC/LoadingWait";
import { callPopUpMessage } from "UI/Messages";

const SignUp = ( {
	FormSignUp,
	ChangeInitialDataSign,
	loading,
	LoadingStart,
	LoadingFailure,
	RegisterSend
} ) => {
	const [ btnDisabled, setBtnDisabled ] = useState(false);
	
	useEffect(() => {
		if (dataValidLogin(FormSignUp)) {
			setBtnDisabled(false);
		} else {
			setBtnDisabled(true);
		}
	}, [
		JSON.stringify(FormSignUp)
	]);

	const handleFormChange = (e) => {
		const { name, value } = e.target;

		if ( !notAllowed(name, value) ) return;
		ChangeInitialDataSign(isValid(name, value, FormSignUp).valid, name, "valid");
		ChangeInitialDataSign(isValid(name, value, FormSignUp).message, "isValidMessage");
		ChangeInitialDataSign(value, name, "value");
	};
	const handlePhoneCountryChange = (name, value) => {
		ChangeInitialDataSign(isValid(name, value).valid, name, "valid");
		ChangeInitialDataSign(value, name, "value");
		ChangeInitialDataSign(isValid(name, value).message, "isValidMessage");
	};

	const HandleSubmitSign = async (e) => {
		e.preventDefault();

		if (!btnDisabled) {
			const Data = getData(FormSignUp);

			LoadingStart();
			const message = await RegisterSend(Data);

			LoadingFailure();
			if ( message.status >= 300 ){
				(async () => {
					try {
						if (message.status >= 500){
							throw new Error(`Error Server ${message.status}`);
						}
						const feedBack = await message.json();
						const messageFeed = CheckKeysName(feedBack);
						
						if (messageFeed){
							messageFeed.forEach(item => callPopUpMessage.error(`${item.name}: ${item.data}`));
						}else{
							window.location.replace("/verifedPhone");
						}
					} catch (error) {
						callPopUpMessage.error(error.message);
					}
				})();
				return;
			};
			window.location.replace("/verifedPhone");
		};
	};

	return (
		<LoadingWait loading = {loading}>
			<SignUpUI 
				FormSignUp = {FormSignUp}
				handlePhoneCountryChange = {handlePhoneCountryChange}
				handleFormChange = {handleFormChange}
				btnDisabled = {btnDisabled}
				HandleSubmitSign = {HandleSubmitSign}
			/>
			{/*<ErrorMessage>{FormSignUp.isValidMessage}</ErrorMessage>*/}
		</LoadingWait>
	);
};

const mapStateToProps = state => ({
	FormSignUp: state.FormSignUp,
	loading: state.Loading.loading
});

const mapDispatch = dispatch => ({
	ChangeInitialDataSign: (data, name, key) => dispatch( ChangeInitialDataSign( data, name, key ) ),
	RegisterSend: data => dispatch( SignUpAction.RegisterSend(data) ),
	LoadingStart: () => dispatch( Loading.SetStartLoading() ),
	LoadingFailure: () => dispatch( Loading.SetExitLoading() )
});

export default connect( mapStateToProps, mapDispatch )( SignUp );
