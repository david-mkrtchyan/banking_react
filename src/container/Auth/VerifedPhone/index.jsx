import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { ChangeInitialDataPin, VerifedPinAction, Loading } from "Action";
import { isValid, notAllowed, dataValidLogin, getDataPin } from "Engine/Validation";

import LoadingWait from "HOC/LoadingWait";
import VerifedPhoneUI from "~c/FormBuilder/VerifedPhone";
import { callPopUpMessage, ErrorMessage } from "UI/Messages";
import { RemoveSession } from "BrowserStorage";

const VerifedPhone = ({
	FormPin,
	ChangeInitialDataPin,
	loading,
	LoadingStart,
    LoadingFailure,
    SendPinCode
}) => {
	const [ btnDisabled, setBtnDisabled ] = useState(false);

	useEffect(() => {
		if (dataValidLogin(FormPin)) {
			setBtnDisabled(false);
		} else {
			setBtnDisabled(true);
		}
	}, [
		JSON.stringify(FormPin)
	]);
	
	const handleFormChange = (e) => {
		const { name, value } = e.target;

		if ( !notAllowed(name, value) ) return;
		ChangeInitialDataPin(isValid(name, value, FormPin).valid, name, "valid");
		ChangeInitialDataPin(isValid(name, value).message, "isValidMessage");
		ChangeInitialDataPin(value, name, "value");
    };
    
	const HandleSubmitPin = async (e) => {
		e.preventDefault();

		if (!btnDisabled) {
			const Data = getDataPin(FormPin);

			LoadingStart();
			const message = await SendPinCode(Data);

			LoadingFailure();
			if ( message.status >= 300 ){
				(async () => {
					try {
						if (message.status >= 500){
							throw new Error(`Error Server ${message.status}`);
						}
						const feedBack = await message.json();

						if(feedBack.phone_number){
							feedBack.phone_number.forEach(item => callPopUpMessage.error(item))
						}
						if(feedBack.non_field_errors){
							feedBack.non_field_errors.forEach(item => callPopUpMessage.error(item));
						}
					} catch (error) {
						callPopUpMessage.error(error.message);
					}
				})();
				return;
			};
			//window.location.replace("/login");
		};
	};

	return (
		<LoadingWait loading = {loading}>
			<VerifedPhoneUI 
				FormPin = {FormPin}
				handleFormChange = {handleFormChange}
				btnDisabled = {btnDisabled}
				HandleSubmitPin = {HandleSubmitPin}
			/>
			<ErrorMessage>{FormPin.isValidMessage}</ErrorMessage>
		</LoadingWait>
	);
};

VerifedPhone.propTypes = {
	ChangeInitialDataPin: propTypes.func,
	FormPin: propTypes.object,
	loading: propTypes.bool,
	LoadingFailure: propTypes.func,
	LoadingStart: propTypes.func,
	SendPinCode: propTypes.func
};

const mapStateToProps = state => ({
	FormPin: state.FormPin,
	loading: state.Loading.loading
});
const mapDispatchToProps = dispatch => ({
	ChangeInitialDataPin: (data, name, key) => dispatch(ChangeInitialDataPin( data, name, key )),
	SendPinCode: data => dispatch( VerifedPinAction.SendPinCode(data) ),
	LoadingStart: () => dispatch( Loading.SetStartLoading() ),
	LoadingFailure: () => dispatch( Loading.SetExitLoading() )
});

export default connect( mapStateToProps, mapDispatchToProps )( VerifedPhone );