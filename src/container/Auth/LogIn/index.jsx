import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { isValid, notAllowed, dataValidLogin, getData, getDataPinLogin } from "Engine/Validation";
import { ChangeInitialData, Login, Loading, SetModalChoiceView, ChangeDataLogInPin } from "Action";
import LoadingWait from "HOC/LoadingWait";
import LogInUI from "~c/FormBuilder/LogIn";
import LogInPinUI from "~c/FormBuilder/LogInPin";
import { callPopUpMessage } from "UI/Messages";
import { SetLocal } from "BrowserStorage";

const LogIn = ({
	FormLogin,
	ChangeInitialData,
	loginAction,
	loading,
	LoadingStart,
	LoadingFailure,
	LoginSuccess,
	PinIsVerifed,
	ChangeDataLogInPin,
	FormPin,
	SetModalChoiceView
}) => {
	const [ btnDisabled, setBtnDisabled ] = useState(false);
	const [ FeedBack, setFeedBack ] = useState(true);

	useEffect(() => {
		if (dataValidLogin(FormLogin) || dataValidLogin(FormPin)) {
			setBtnDisabled(false);
		} else {
			setBtnDisabled(true);
		}
	}, [
		JSON.stringify(FormLogin),
		JSON.stringify(FormPin)
	]);
	
	const handleFormChange = (e) => {
		const { name, value } = e.target;
		console.log(value, 'handleFormChange')

		if ( !notAllowed(name, value) ) return;
		ChangeInitialData(isValid(name, value).valid, name, "valid");
		ChangeInitialData(isValid(name, value).message, "isValidMessage");
		ChangeInitialData(value, name, "value");
	};
	const handlePhoneChange = (name, value) => {
		console.log(value, 'value')
		ChangeInitialData(isValid(name, value).valid, name, "valid");
		ChangeInitialData(value, name, "value");
		ChangeInitialData(isValid(name, value).message, "isValidMessage");
	};
	const handleFormChangePin = (data) => {
		ChangeDataLogInPin(data, "password", "value");
	}


	const HandleSubmitLog = async (e) => {
		e.preventDefault();

		if (!btnDisabled) {
			const Data = getData(FormLogin);
			localStorage.setItem("_pn", Data.phone_number);
			LoadingStart();
			const message = await loginAction(Data);
			LoadingFailure();
			if ( message.status >= 300 ){
				(async () => {
					try {
						if (message.status >= 500){
							throw new Error(`Error Server ${message.status}`);
						}
						const feedBack = await message.json();
						
						if(feedBack["phone_verified"] === "False"){
							SetModalChoiceView(true);
						};
						callPopUpMessage.error(feedBack.detail);
					} catch (error) {
						callPopUpMessage.error(error.message);
					}
				})();
				return;
			};
		

			if (message.status >= 200 && message.status < 300){
				LoginSuccess();
				const newDate = + new Date();
				SetLocal("_dm", newDate + "");
				SetLocal("_la", newDate + "");
				window.location.replace("/");
				return;
			};
			SetModalChoiceView(true);
		};
	};

	const HandleSubmitLogPin = async (data) => {

		if (!btnDisabled) {
			const Data = getDataPinLogin(data);
			LoadingStart();
			const message = await loginAction(Data);

			LoadingFailure();
			if ( message.status >= 300 ){
				(async () => {
					try {
						setFeedBack(false);
						if (message.status >= 500){
							throw new Error(`Error Server ${message.status}`);
						}
						const feedBack = await message.json();

						callPopUpMessage.error(feedBack.detail);
					} catch (error) {
						callPopUpMessage.error(error.message);
					}
				})();
				return;
			}
			if (message.status >= 200 && message.status < 300){
				LoginSuccess();
				window.location.replace("/");
				return;
			}
		}
	}

	return (
		<>
			{ !PinIsVerifed ? 
				<>
				<LoadingWait loading = {loading}>
					<LogInUI 
						FormLogin = {FormLogin}
						handlePhoneChange = {handlePhoneChange}
						handleFormChange = {handleFormChange}
						btnDisabled = {btnDisabled}
						HandleSubmitLog = {HandleSubmitLog}
						PinIsVerifed = {PinIsVerifed}
					/> 
					{/* <ErrorMessage>{FormLogin.isValidMessage}</ErrorMessage> */}
				</LoadingWait>
				</>
				:
				<>
					<LogInPinUI
						HandleSubmitLog = {HandleSubmitLogPin}
						btnDisabled = {btnDisabled}
						FormPin = {FormPin}
						handleFormChangePin = {handleFormChangePin}
						FeedBack = {FeedBack}
						setFeedBack = {setFeedBack}
					/>
					{/* <ErrorMessage>{FormPin.isValidMessage}</ErrorMessage> */}
				</>
			}
		</>
	);
};

LogIn.propTypes = {
	ChangeInitialData: propTypes.func,
	FormLogin: propTypes.object,
	loading: propTypes.bool,
	LoadingFailure: propTypes.func,
	LoadingStart: propTypes.func,
	loginAction: propTypes.func,
	LoginSuccess: propTypes.func,
	PinIsVerifed: propTypes.bool
};

const mapStateToProps = state => ({
	FormLogin: state.FormLogin,
	FormPin: state.FormLoginPin,
	loading: state.Loading.loading,
	PinIsVerifed: state.PinIsVerifed.isVerifed
});
const mapDispatchToProps = dispatch => ({
	ChangeInitialData: (data, name, key) => dispatch(ChangeInitialData( data, name, key )),
	loginAction: data => dispatch( Login.loginAction(data) ),
	LoadingStart: () => dispatch( Loading.SetStartLoading() ),
	LoadingFailure: () => dispatch( Loading.SetExitLoading() ),
	LoginSuccess: () => dispatch( Login.Success() ),
	SetModalChoiceView: view => dispatch( SetModalChoiceView(view) ),
	ChangeDataLogInPin: (data, name, key) => dispatch(ChangeDataLogInPin( data, name, key ))
});

export default connect( mapStateToProps, mapDispatchToProps )( LogIn );
