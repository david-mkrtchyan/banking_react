import { CHANGE_EXCHANGE_FORM, CHANGE_ALL_EXCHANGE_FORM } from "Constants";
import PostHeader from "Method/postHeader";
import { ExchangeDataUrl, ExchangeURL } from "HTTP";
import { GetTokenAccess, SetSession } from "BrowserStorage";

import { urlBuilder } from "Routes";
//Encription
import { Encription } from "Engine/Encryption";
import { DelSpaces } from "Engine";

export const ChangeExchangeForm = ( Data, Name ) => ({
	type: CHANGE_EXCHANGE_FORM,
	Name,
	Data
});


export const ChangeAllExchangeForm = (data) => ({
	type: CHANGE_ALL_EXCHANGE_FORM,
	data
});

export const GetExchangeData = data => async (dispatch) => {
	const AccessToken = GetTokenAccess();
	const Req = PostHeader( ExchangeDataUrl, AccessToken, {
		amount: data.amount,
		base_currency: data.base_currency,
		swap_currency: data.swap_currency
	});
	const CloneReq = Req;

	CloneReq
		.then(req => {
			if (req.status >= 300){
				throw new Error(req);
			}
			return req.json();
		})
		.then(res => {
			 let { price, appExchange} = res.message;
			 dispatch(ChangeExchangeForm( price, "price"));
			 dispatch(ChangeExchangeForm( appExchange, "appExchange"));
		})
		.catch(() => Req);

	return Req;
};

export const GoToExchangeConfirm = data => async (dispatch) => {
	console.log(data);
	SetSession("_ex", Encription(JSON.stringify(data), "_ex"));
	window.location.replace(urlBuilder("confirmExchange", {data: Encription(JSON.stringify(data), "data")}));
};

export const TransactionExchange = (data) => async () => {
	const AccessToken = GetTokenAccess();
	const SendData = {
		amount: data.amount,
		base_currency: data.base_currency,
		swap_currency: data.swap_currency
	};
	const Req = PostHeader( ExchangeURL, AccessToken, SendData );
	const CloneReq = Req;

	CloneReq
		.then(req => {
			if (req.status >= 300){
				throw new Error(req);
			};
			return req.json()
		})
		.then(res => {
			console.log(res)
			const DataLink = {
				"success": res.success,
				"message": res.message,
				"amount": data.amount,
				"price": data.price,
				"base_currency": data.base_currency,
				"swap_currency": data.swap_currency,
				"appExchange": data.appExchange
			};
			SetSession("_ex", Encription(JSON.stringify(DataLink), "_ex"));
			window.location.replace(urlBuilder("confirmationExchange", {data: Encription(JSON.stringify(DataLink), "data")}));
		})
		.catch(() => Req);

	return Req;

};