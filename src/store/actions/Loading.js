import { START_LOADING, EXIT_LOADING } from "Constants";

export const SetStartLoading = () => ({
	type: START_LOADING
});

export const SetExitLoading = () => ({
	type: EXIT_LOADING
});
