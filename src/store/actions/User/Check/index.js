import { CHECK_USER_TIME } from "Constants";
import { CheckUserURL } from "HTTP";
import Post from "Method/post";
import { SetLocal } from "BrowserStorage";
import { Encription } from "Engine/Encription";

export const checkUser = send => (
    async dispatch => {
        if (send) {
            const Req = await Post(CheckUserURL, {refresh: send});
            const CloneReq = Req;

            CloneReq
                .then(res => res.json())
                .then(res => localStorage.setItem("token", JSON.stringify({...JSON.parse(localStorage.getItem("token")), ...{access: res.access}})));

            dispatch({
                type: CHECK_USER_TIME,
                validation: CloneReq
            });
        }       
    }
);