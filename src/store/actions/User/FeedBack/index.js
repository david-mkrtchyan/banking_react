import { FEED_BACK_USER } from "Constants";
import { GetTokenAccess } from "BrowserStorage";
import { FeedBackURL } from "HTTP";
import PostHeader from "Method/postHeader";

export const CheckUserFeedBack = () => async dispatch => {
    const AccessToken = GetTokenAccess();
    const Req = PostHeader( FeedBackURL, AccessToken ); 
    const CloneReq = Req;

    CloneReq
        .then(req => {
            if (req.status >= 300) {
                throw new Error(req)
            };
            return req.json()
        })
        .then(res => {
            dispatch({
                type: FEED_BACK_USER,
                messages: res.message,
            })
        })
        .catch(() => Req);

    return Req;
};