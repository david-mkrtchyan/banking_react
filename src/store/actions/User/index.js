import { SEND_PROFIL_CURRENCY } from "Constants";

export const ProfilSendCurrency = currency => ({
    type: SEND_PROFIL_CURRENCY,
    currency
});