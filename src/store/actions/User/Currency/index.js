import { SEND_CURRENCY_AMOUNT } from "Constants";
import PostHeader from "Method/postHeader";
import { ProfilSendCurrency } from "Action/User";
import { UserCurrencyURL, UserCheckingURL, TransactionUserURL } from "HTTP";
import { GetLocal, GetTokenAccess } from "BrowserStorage";
import { Decoding } from "Engine/Encryption";

export const CheckUserCurrency = () => (
    async dispatch => {
        const AccessToken = GetTokenAccess();
        const Req = PostHeader( UserCurrencyURL, AccessToken ); 
        const CloneReq = Req;
        CloneReq
            .then(req => {
                if (req.status >= 300) {
                    throw new Error(req)
                };
                return req.json()
            })
            .then(res => {
                dispatch(ProfilSendCurrency(res));
            })
            .catch(() => Req);
    
        return Req;
    }
);

export const ChangeDataCurrency = (Data, Name, Key) => async dispatch => dispatch({
	type: SEND_CURRENCY_AMOUNT,
	Name,
	Data,
	Key
});

export const SendCurrencyAmount = data => (
    async () => {
        const AccessToken = JSON.parse(Decoding(GetLocal("_t"), "_t")).access;
        const body = {phone_number:data.phone_number.value};
        const Req = PostHeader( UserCheckingURL, AccessToken, body );

        const CloneReq = Req;

        return CloneReq
    }
);

export const TransferCurrencyAmount = data => (
	async () => {
        const AccessToken = JSON.parse(Decoding(GetLocal("_t"), "_t")).access;
        const body = {phone_number: data.phone_number, account_id: +data.account_id, amount: data.amount};
        const Req = PostHeader( TransactionUserURL, AccessToken, body );

		const CloneReq = Req;

		return CloneReq;
	}
);
