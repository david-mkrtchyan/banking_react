import { VERIFED_SUCCESS } from "Constants";
import Post from "Method/post";
import { PostPinURL } from "HTTP";
import { SetSession, SetLocal } from "BrowserStorage";
import { Encription } from "Engine/Encryption";

export const SendPinCode = data => async dispatch => {
	const Req = Post(PostPinURL, data);
	const CloneReq = Req;

	CloneReq
		.then((req) => {
			if (req.status >= 300){
				throw new Error(req);
			}
			return req.json();
		})
		.then((res) => {
			dispatch({
				type: VERIFED_SUCCESS
			});
			if(res.success){
				SetSession("_vm", Encription(res.message, "_vm"));
				SetLocal("_vs", Encription("true", "_vs"));
				SetSession("_vs", Encription("true", "_vs"));
				window.location.replace("/login");
			};
		})
		.catch(() => Req);
	
	return Req;
};
