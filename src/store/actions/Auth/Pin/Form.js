import { FORM_DATA_CHANGE_PIN } from "Constants";

export const ChangeInitialDataPin = (Data, Name, Key) => async dispatch => dispatch({
	type: FORM_DATA_CHANGE_PIN,
	Name,
	Data,
	Key
});
