import Post from "Method/post";
import { TokenPostURL } from "HTTP";
import { SetLocal } from "BrowserStorage";
import { Encription } from "Engine/Encryption";

import {
	LOGIN_SUCCESS,
	LOGIN_FAILURE
} from "Constants";

//Login

export const Success = () => ({
	type: LOGIN_SUCCESS
});
  
export const Failure = error => ({
	type: LOGIN_FAILURE,
	error
});

export const loginAction = data => async () => {
	const Req = Post(TokenPostURL, data);
	const CloneReq = Req;
	CloneReq
		.then((req) => {
			if (req.status >= 300){
				throw new Error(req);
			};
			return req.json();
		})
		.then((res) => {
			SetLocal("_t", Encription(JSON.stringify(res), "_t"));
			SetLocal("_u", Encription(`${ res["verify_account"] }`, "_u"));
		})
		.catch(() => Req);
	return Req;
};

//Logout

export const OutSuccess = () => ({
	type: LOGIN_FAILURE
});

export const OutFailure = () => ({
	type: LOGIN_FAILURE
});

export const LogOutAction = () => {
	localStorage.removeItem("_t");
	localStorage.removeItem("_u");
	localStorage.removeItem("_la");
	SetLocal("_vs", Encription("true", "_vs"))

	// window.location.href = "/login";
	window.location.replace("/login");
};

export const LogOutWithoutToken = () => async dispatch => {
	localStorage.removeItem("_t");
	localStorage.removeItem("_u");
	localStorage.removeItem("_la");
	SetLocal("_vs", Encription("true", "_vs"));

	window.location.href = "/login";
};