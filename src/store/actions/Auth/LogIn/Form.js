import { FORM_DATA_CHANGE, FORM_DATA_CHANGE_LOGIN_PIN } from "Constants";

export const ChangeInitialData = (Data, Name, Key) => async dispatch => dispatch({
	type: FORM_DATA_CHANGE,
	Name,
	Data,
	Key
});

export const ChangeDataLogInPin = (Data, Name = "password", Key = "value") =>({
	type: FORM_DATA_CHANGE_LOGIN_PIN,
	Name,
	Data,
	Key
});