import { REGISTER_SUCCESS, REGISTER_FAILURE } from "Constants";
import Post from "Method/post";
import { RegisterURL } from "HTTP";
import { SetSession, SetLocal } from "BrowserStorage";
import { Encription } from "Engine/Encryption";

export const RegisterSuccess = () => ({
	type: REGISTER_SUCCESS
});

export const RegisterFailure = error => ({
	type: REGISTER_FAILURE,
	error
});

export const RegisterSend = data => async () => {
	const Req = Post(RegisterURL, data);
	const CloneReq = Req;

	CloneReq
		.then((req) => {
			if (req.status >= 300){
				throw new Error(req);
			};
			return req.json();
		})
		.then((res) => {
			res["next-step"] && SetLocal("_pn", data.phone_number);
			res["next-step"] && SetSession("_sm", Encription(res.message,"_sm"))
			SetSession("_rs", Encription(`${res["next-step"]}`,"_rs"));
			//window.location.replace("/verifedPhone");
		})
		.catch(() => Req);
	
	return Req;
};
