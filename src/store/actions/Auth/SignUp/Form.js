import { FORM_DATA_SIGN_CHANGE } from "Constants";

export const ChangeInitialDataSign = (Data, Name, Key) => async dispatch => dispatch({
	type: FORM_DATA_SIGN_CHANGE,
	Name,
	Data,
	Key
});
