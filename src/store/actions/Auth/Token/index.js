
import { CheckUserURL } from "HTTP";
import { Decoding, Encription } from "Engine/Encryption";
import { GetLocal, SetLocal, GetTokenRefresh, GetToken } from "BrowserStorage";
import Post from "Method/post";


export const SendRefreshTokenUser = () => async () => {
	const Token = GetToken();
	const RefreshToken = GetTokenRefresh();
	const Req = Post( CheckUserURL, {refresh: RefreshToken} );
	const CloneReq = Req;
	CloneReq
		.then((req) => {
			if (req.status >= 300) {
				throw new Error(req);
			};
			return req.json();
		})
		.then((res) => {
            const TokenEncription = Encription(JSON.stringify({
                ...Token,
                ...res
			}), "_t");
            SetLocal("_t", TokenEncription);
		})
		.catch(() => Req);

	return Req;
};
