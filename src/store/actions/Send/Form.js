import { SEND_CURRENCY_ANOTHER_USER, CHANGE_ALL_FORM_DATA_SEND_USER } from "Constants";
import PostHeader from "Method/postHeader";
import { UserCheckingURL, TransactionUserURL } from "HTTP";
import { GetTokenAccess, SetSession } from "BrowserStorage";

import { urlBuilder } from "Routes";
//Encription
import { Encription } from "Engine/Encryption";
import { DelSpaces } from "Engine";

export const ChangeAnotherUserForm = ( Data, Name ) => ({
	type: SEND_CURRENCY_ANOTHER_USER,
	Name,
	Data
});

export const ChangeAllFormAnother = (data) => ({
		type: CHANGE_ALL_FORM_DATA_SEND_USER,
		data
});

export const CheckUserPhoneHas = data => async (dispatch) => {
	const AccessToken = GetTokenAccess();

	const Req = PostHeader( UserCheckingURL, AccessToken, {phone_number:data.phone_number} ); 

	const CloneReq = Req;

	CloneReq
		.then(req => {
			if (req.status >= 300){
				throw new Error(req);
			};
			return req.json()
		})
		.then(res => {
			const SendUserAmount = {...data, ...res.user};		
			SetSession("_su", Encription(JSON.stringify(SendUserAmount), "_su"));
			dispatch(ChangeAllFormAnother(SendUserAmount));
			window.location.replace(urlBuilder("confirmSendOtherUser", {data: Encription(JSON.stringify(SendUserAmount), "data")}));
		})
		.catch(() => Req);

	return Req;
};

export const TransactionUserToUser = (data) => async () => {
	const AccessToken = GetTokenAccess();
	const SendData = {
		phone_number: data.phone_number, 
		account_id: +data.account_id, 
		amount: data.amount,
		message: data.message
	};
	const Req = PostHeader( TransactionUserURL, AccessToken, SendData ); 
	const CloneReq = Req;

	CloneReq
		.then(req => {
			if (req.status >= 300){
				throw new Error(req);
			};
			return req.json()
		})	
		.then(res => {		
			const DataLink = {
				"amount":data.amount,
				"phone_number":data.phone_number, 
				"first_name":data.first_name,
				"last_name":data.last_name,
				"currency":data.currency,
				"message":data.message,
				"success":res.success,
			};
			SetSession("_su", Encription(JSON.stringify(DataLink), "_su"));	
			window.location.replace(urlBuilder("confirmationSendOtherUser", {data: Encription(JSON.stringify(DataLink), "data")}));
		})
		.catch(() => Req);

	return Req;

};