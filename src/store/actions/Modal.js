import { MODAL_CHOICE_VIEW } from "Constants";

export const SetModalChoiceView = view => ({
	type: MODAL_CHOICE_VIEW,
	view
});