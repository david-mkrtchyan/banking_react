import { HEADER_HEIGTH } from "Constants";

export const SetHeaderHeight = heigth => ({
	type: HEADER_HEIGTH,
	heigth
});
