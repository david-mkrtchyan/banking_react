import { START_LOADING, EXIT_LOADING } from "Constants";

const initialState = {
	loading: false
};

const reducer = ( state = initialState, action ) => {
	switch (action.type) {
		case START_LOADING:
			return {
				...state,
				loading: true
			};
		case EXIT_LOADING: 
			return {
				...state,
				loading: false
			};
		default:
			return state;
	}
};

export default reducer;
