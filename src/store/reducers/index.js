import { combineReducers } from "redux";
import Modal from "./Modal";
import LayoutSize from "./LayoutSize";
import LogIn from "./Auth/LogIn";
import FormPin from "./Auth/Pin/Form";
import PinIsVerifed from "./Auth/Pin";
import FormLogin from "./Auth/LogIn/Form";
import FormLoginPin from "./Auth/LogIn/FormPin";
import Loading from "./Loading";
import FormSignUp from "./Auth/SignUp/Form";
import FormCurrency from "./User/Currency/Form";
import Profil from "./User";
import FeedBackMessages from "./User/FeedBack";
import SendAnotherProfil from "./Send";
import ExchangeData from "./Exchange";

export default combineReducers({
	LayoutSize,
	LogIn,
	FormLogin,
	FormLoginPin,
	Loading,
	FormSignUp,
	FormPin,
	PinIsVerifed,
	Modal,
	FormCurrency,
	Profil,
	FeedBackMessages,
	SendAnotherProfil,
	ExchangeData
});
