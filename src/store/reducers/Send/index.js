import { SEND_CURRENCY_ANOTHER_USER, CHANGE_ALL_FORM_DATA_SEND_USER } from "Constants";

const initialState = {
    "phone_number": "",
    "message": "",
    "amount": 0,
    "currency": "",
    "account_id": 0,
    "first_name": "",
    "last_name": "",

    get full_name(){
        console.log(this)
        return `${this.first_name}  ${this.last_name}`
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case SEND_CURRENCY_ANOTHER_USER: {
			const { Data, Name } = action;

            return {
				...state,
				[Name]: Data
			};
        };
        case CHANGE_ALL_FORM_DATA_SEND_USER:
            const Data = action.data;
            return {
                ...state,
                ...Data
            }
        default:
            return state;
    }
};

export default reducer;
