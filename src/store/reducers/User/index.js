import { SEND_PROFIL_CURRENCY } from "Constants";

const initialState = {
    currency: [],
    profil_name: "",
    profil_last_name: "",
};

const Profil = (state = initialState, action) => {
    switch(action.type){
		case SEND_PROFIL_CURRENCY:
			return {
				...state,
				currency: action.currency
            };
        default: 
            return state;
    }
};

export default Profil;
