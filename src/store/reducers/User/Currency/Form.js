import { SEND_CURRENCY_AMOUNT } from "Constants";

const initialState = {
    phone_number: { value: '', valid: null },
    password: { value: '', valid: null },
    isValidMessage: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case SEND_CURRENCY_AMOUNT: {
			const { Key, Data, Name } = action;
			if (Key) {
				const CloneSettings = state[Name];
				const newValue = { 
					...CloneSettings,
					[Key]: Data
				};

				return {
					...state,
					[Name]: newValue
				};
			}
			return {
				...state,
				[Name]: Data
			};
		};
        default:
            return state;
    }
};

export default reducer;
