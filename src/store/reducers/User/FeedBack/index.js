import { FEED_BACK_USER } from "Constants";

const initialState = {
    messages: []
};

const reducer = (state = initialState, action ) => {
    switch(action.type){
        case FEED_BACK_USER:
            return {
                ...state,
                messages: action.messages
            };
        default:
            return state
    }
};

export default reducer;
