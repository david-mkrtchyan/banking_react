import { FORM_DATA_CHANGE } from "Constants";

const initialState = {
	phone_number: { value: "", valid: null },
	password: { value: "", valid: null },
	isValidMessage: ""
};

const FormLogin = (state = initialState, action) => {
	switch (action.type){
		case FORM_DATA_CHANGE: {
			const { Key, Data, Name } = action;
			
			if (Key) {
				const CloneSettings = state[Name];
				const newValue = { 
					...CloneSettings,
					[Key]: Data
				};

				return {
					...state,
					[Name]: newValue
				};
			}
			return {
				...state,
				[Name]: Data
			};
		};
		default:
			return state;
	}
};

export default FormLogin;
