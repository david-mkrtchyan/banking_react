import { FORM_DATA_CHANGE_LOGIN_PIN } from "Constants";

const initialState = {
	password: { value: "", valid: null },
	isValidMessage: ""
};	

const FormLogin = (state = initialState, action) => {
	switch (action.type){
		case FORM_DATA_CHANGE_LOGIN_PIN: {
			const { Data, Name, Key  } = action;
			if (Key) {
				const CloneSettings = state[Name];
				const newValue = { 
					...CloneSettings,
					[Key]: Data
				};

				return {
					...state,
					[Name]: newValue
				};
			}

			return {
				...state,
				[Name]: Data
			};
		};
		default:
			return state;
	}
};

export default FormLogin;
