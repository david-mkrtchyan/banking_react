import { GetLocal } from "BrowserStorage";
import { Decoding } from "Engine/Encryption";


import {
	LOGIN_SUCCESS,
	LOGIN_FAILURE
} from "Constants";

const initialState = {
	isLoged: GetLocal("_u") ? Decoding(GetLocal("_u"), "_u" ) === "true" ? true : false : false 
};

const reducer = ( state = initialState, action ) => {
	switch (action.type){
		case LOGIN_SUCCESS:
      		return {
				...state,
				isLoged: true 
      		};
		case LOGIN_FAILURE:
			return {
				...state,
				isLoged: false
			};
		default: 
			return state;
	}
};

export default reducer;
