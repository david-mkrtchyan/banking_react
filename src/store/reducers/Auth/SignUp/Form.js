import { FORM_DATA_SIGN_CHANGE } from "Constants";

const initialState = {
	email: { value: "", valid: null },
	first_name: { value: "", valid: null },
	last_name: { value: "", valid: null  },
	phone_number: { value: "", valid: null },
	country_code: { value: "", valid: null },
	password: { value: "", valid: null },
	password2: { value: "", valid: null },
	isValidMessage: ""
};

const FormSignUp = (state = initialState, action) => {
	switch (action.type){
		case FORM_DATA_SIGN_CHANGE: {
			const { Key, Data, Name } = action;
			
			if (Key) {
				const CloneSettings = state[Name];
				const newValue = { 
					...CloneSettings,
					[Key]: Data
				};

				return {
					...state,
					[Name]: newValue
				};
			}
			return {
				...state,
				[Name]: Data
			};
		};
		default:
			return state;
	}
};

export default FormSignUp;
