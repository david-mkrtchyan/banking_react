//import { REGISTER_SUCCESS, REGISTER_FAILURE } from "Constants";

const initialState = {
	loading: false, 
	error: null
};

const reducer = (state = initialState, action) => {
	switch (action.type){
		default: 
			return state;
	}
};

export default reducer;
