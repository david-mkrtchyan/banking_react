import { VERIFED_SUCCESS, VERIFED_FAILURE } from "Constants";

import { GetLocal } from "Engine/BrowserStorage";
import { Decoding } from "Engine/Encryption";

const initialState = {
	isVerifed: GetLocal("_vs") ? Decoding(GetLocal("_vs"), "_vs") === "true" ? true : false : false,
};

const reducer = (state = initialState, action) => {
	switch (action.type){
		case VERIFED_SUCCESS:
			return {
				...state,
				isVerifed: true
			};
		case VERIFED_FAILURE:
			return {
				...state,
				isVerifed: false
			};
		default: 
			return state;
	}
};

export default reducer;
