import { FORM_DATA_CHANGE_PIN } from "Constants";

import { GetSession } from "Engine/BrowserStorage";
import { Decoding } from "Engine/Encryption";

const initialState = {
    phone_pin: {value: "", valid: null},
	pin: {value: "", valid: null},
	pin2: {value: "", valid: null},
	isValidMessage: ""
};

const reducer = (state = initialState, action) => {
	switch (action.type){
        case FORM_DATA_CHANGE_PIN: {
			if( GetSession("_rs") ) {
				const { Key, Data, Name } = action;
				if ( Decoding(GetSession("_rs"),"_rs") !== "true") {
					return {
						phone_pin: {value: "", valid: null},
						pin: {value: "", valid: null},
						pin2: {value: "", valid: null},
						isValidMessage: ""
					};
				};
				if (Key) {
					const CloneSettings = state[Name];
					const newValue = { 
						...CloneSettings,
						[Key]: Data
					};

					return {
						...state,
						[Name]: newValue
					};
				}
				return {
					...state,
					[Name]: Data
				};
			};	
		};
		default: 
			return state;
	}
};

export default reducer;
