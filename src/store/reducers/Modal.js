import { MODAL_CHOICE_VIEW } from "Constants";

const initialState = {
	ModalChoice: false
};

const reducer = (state = initialState, action) => {
	switch (action.type){
		case MODAL_CHOICE_VIEW:
			return {
				...state,
				ModalChoice: action.view
			};
		default: 
			return state;
	}
};

export default reducer;
