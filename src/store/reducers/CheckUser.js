import {CHECK_USER_IN_TIME} from "Constants";

const initialState = {
    checkUser: localStorage.getItem("token") ? (JSON.parse(localStorage.getItem("token")).access) ? true : false : false
};

const Check = (state = initialState, action) => {
    switch(action.type){
        case CHECK_USER_IN_TIME:
            return {
                ...state,
                checkUser: action.validation
            }
        default:
            return state;
    }
};

export default Check;