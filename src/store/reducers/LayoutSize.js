import { HEADER_HEIGTH } from "Constants";

const initialState = {
	headerSize: 0
};

const reducer = (state = initialState, action) => {
	switch (action.type){
		case HEADER_HEIGTH:
			return {
				...state,
				headerSize: action.heigth
			};
		default: 
			return state;
	}
};

export default reducer;
