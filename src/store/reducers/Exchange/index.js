import { CHANGE_EXCHANGE_FORM, CHANGE_ALL_EXCHANGE_FORM } from "Constants";

const initialState = {
    "message": "",
    "amount": 0,
    "price": 0,
    "base_currency": "",
    "swap_currency": "",
    "appExchange": 1
};

const reducer = (state = initialState, action) => {
	console.log(action)
    switch (action.type){
        case CHANGE_EXCHANGE_FORM: {
          const { Data, Name } = action;
            return {
            ...state,
            [Name]: Data
          }
        }
			case CHANGE_ALL_EXCHANGE_FORM:
				const Data = action.data;
				return {
					...state,
					...Data
				};
        default:
            return state;
    }
};

export default reducer;
