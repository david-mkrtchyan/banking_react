import React from "react";
import PropTypes from "prop-types";

const Icon = ({ name, size, viewBox, children, className }) => (
	<svg id = {name} width = {size} height = {size} viewBox = {viewBox} className = {className}>
		{children}
	</svg>
);

Icon.propTypes = {
	name: PropTypes.string.isRequired,
	size: PropTypes.string.isRequired,
	viewBox: PropTypes.string.isRequired
};

export const UserSVG = props => (
	<Icon {...props} viewBox="0 0 285.5 285.5" >
		 <g stroke="none">
			<path fill = "#fff" d = "M143 126a63 63 0 100-126 63 63 0 000 126zm0-96a33 33 0 110 66 33 33 0 010-66zM143 156c-64 0-115 51-115 115 0 8 6 15 15 15h200c8 0 15-7 15-15 0-64-52-115-115-115zM59 256a85 85 0 01167 0H59z"/>
		</g>
	</Icon>
);

export const EmailSVG = props => (
	<Icon {...props} viewBox="0 0 493.3 493.3" >
		<path fill = "#fff" d = "M353 451c3 10-2 20-12 24-36 14-69 18-113 18C111 493 7 409 7 270 7 125 112 0 272 0c125 0 214 86 214 205 0 103-58 169-134 169-34 0-58-17-61-55h-2c-22 36-54 55-91 55-46 0-80-34-80-93 0-86 64-165 166-165 19 0 39 3 56 7 17 5 27 21 24 37l-17 105c-7 42-2 61 18 62 31 0 69-39 69-120 0-92-60-164-170-164-108 0-203 85-203 221 0 118 76 185 181 185 28 0 57-4 81-13a23 23 0 0130 15zm-59-270c0-3-2-7-6-7l-15-2c-47 0-84 46-84 101 0 27 12 44 36 44 26 0 54-33 60-75l9-61z" stroke="none"/>
	</Icon>
);

export const LockSVG = props => (
	<Icon {...props} viewBox = "0 0 330 330" >
		<path fill = "#fff" d = "M265 130h-15V85a85 85 0 00-170 0v45H65c-8 0-15 7-15 15v170c0 8 7 15 15 15h200c8 0 15-7 15-15V145c0-8-7-15-15-15zM110 85a55 55 0 01110 0v45H110V85zm140 215H80V160h170v140z" stroke="none"/>
	</Icon>
);

export const LocationSVG = props => (
	<Icon {...props} viewBox = "0 0 330 330" >
		<path fill = "#fff" d = "M149 0C87 0 38 50 38 111c0 91 99 180 104 183a10 10 0 0013 0c5-3 104-92 104-183C259 50 210 0 149 0zm0 273c-23-22-91-93-91-162a90 90 0 01181 0c0 69-68 140-90 162z" stroke="none"/>
		<path fill = "#fff" d = "M149 59a52 52 0 000 103 52 52 0 000-103zm0 83c-17 0-31-14-31-31s14-31 31-31c16 0 30 14 30 31s-14 31-30 31z" stroke="none"/>
	</Icon>
);

export const VerifedPhoneSVG = props => (
    <Icon {...props} viewBox = "0 0 540.6 540.6" >
        <path fill="#fff" d="M384 0H157c-21 0-37 16-37 37v467c0 20 16 37 37 37h227c20 0 37-17 37-37V37c0-21-17-37-37-37zm22 504c0 12-10 22-22 22H157c-13 0-23-10-23-22V37c0-13 10-23 23-23h227c12 0 22 10 22 23v467z"/>
        <path fill="#fff" d="M388 60H152c-2 0-3 1-3 3v410c0 2 1 4 3 4h236c2 0 4-2 4-4V63c0-2-2-3-4-3zm-13 410H165c-5 0-9-4-9-10V76c0-5 4-9 9-9h210c6 0 10 4 10 9v384c0 6-4 10-10 10zM250 38h40a4 4 0 000-7h-40a4 4 0 000 7zM291 485h-41c-9 0-16 7-16 16v2c0 9 7 15 16 15h41c8 0 15-6 15-15v-2c0-9-7-16-15-16zm8 18c0 5-4 8-8 8h-41c-5 0-9-3-9-8v-2c0-5 4-9 9-9h41c4 0 8 4 8 9v2z"/>
        <path fill="#fff" d="M210 327c-1 5-5 9-6 10v3c0 1 1 3 4 3 4 0 22-4 34-13 4-4 7-8 9-8l9 7c8 8 19 13 32 16 5 1 11 5 15 8 11 8 26 11 29 11s4-1 4-3v-2c-1-2-4-5-5-9-2-4 2-9 6-11 16-8 26-22 26-36 0-24-27-43-60-43 0 0 0 0 0 0l-5-8-5-6a78 78 0 00-56-21c-37 0-67 22-67 49 0 17 11 32 30 41 5 2 8 7 6 12zm99-57c-1-2 3-3 9-2 23 3 41 18 41 35 0 15-12 28-32 33-2 1-3 3-2 5l1 7c2 4-1 6-6 4-6-3-11-6-13-11l-3-2c-14-1-26-5-35-12-4-3-3-7 2-9 23-8 38-25 38-45v-3zm-18-19c7 6 10 14 10 23-2 22-25 40-56 41-1 0-3 1-3 3-2 6-10 10-17 13-5 2-8-1-6-5l2-9c0-2-1-4-3-5-22-6-37-21-37-38 0-23 27-42 60-42 20 0 39 7 50 19z"/>
        <circle cx="203" cy="273.3" r="5.2"/>
        <circle cx="238.1" cy="273.3" r="5.2"/>
        <circle cx="273.1" cy="273.3" r="5.2"/>
    </Icon>
);

export const PinSVG = props => (
    <Icon {...props} viewBox = "0 0 59.3 59.3" >
        <path fill = "#fff" d = "M42 0a17 17 0 00-15 25L3 49a5 5 0 007 7l2-3 7 6 3-3-3-3 2-2 3 3 3-3-6-6 13-13 8 1a17 17 0 000-33zm0 27a11 11 0 110-21 11 11 0 010 21z" stroke="none"/>
    </Icon>
);
