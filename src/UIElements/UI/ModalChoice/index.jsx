import React from "react";

import "./styles.css";

const ModalQuestion = ({ handleAgree, handleDisabled, view, title }) => {
    function closeModal(e) {
        e.stopPropagation()
    }

    const divStyle = {
        display: view ? 'flex' : 'none',
    };

    return (
        <div 
            className="modal"
            onClick={ closeModal }
            style={ divStyle }
        >
        <div className="modal-content"
            onClick={ e => e.stopPropagation() }
        >
            <div className="modal-flex">
				<div className = "Content_Body">
					<p> {title} </p>
				</div>
				<div className = "ButtonBlock">
					<button className = "Right" onClick = { handleAgree }> Yes </button>
					<button className = "Wrong" onClick ={ handleDisabled }> No </button> 
				</div>
            </div>

        </div>

      </div>
    );
}

export default ModalQuestion;
