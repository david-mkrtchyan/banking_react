import React from "react";
import propTypes from "prop-types";
import {Container, ButtonStyle} from "./styledButton";

const Button = ({ 
	children, 
	disabled, 
	onClick, 
	Margin = "15px 0",
	Padding = "14px 62px"
}) => (
	<Container Margin = {Margin} >
		<ButtonStyle 
			disabled = {disabled} 
			type = "submit" 
			onClick = {onClick}
			Padding = {Padding}
		>
			{children}
		</ButtonStyle>
	</Container>
);

Button.propTypes = {
	children: propTypes.node,
	disabled: propTypes.bool
};

export default Button;
