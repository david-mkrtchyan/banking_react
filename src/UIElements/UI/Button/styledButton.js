import styled from "styled-components";

export const Container = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    margin: ${props => props.Margin};
`;

export const ButtonStyle = styled.button`
    padding: ${props => props.Padding};
    font-size: 14px;
    border: none;
    outline: none;
    
    cursor: ${props => props.disabled ? "default" : "pointer"} ;
    background-color: #00a8f9!important;
    color: #fff!important;
    border-radius: 50px;
    background-color: ${props => props.disabled ? "#00a8f9" : "#7e95a1"} ;
    &:hover {
        background-color: ${props => props.disabled ? "" : "#31708fd6"};
    }
`;
