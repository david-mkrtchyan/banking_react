import React from "react";
import propTypes from "prop-types";
import { InputField } from "./StyledInput";
import ChooseInput from "Engine/UI/InputSVG";


import "./style.scss";


const Input = ({ 
	type, 
	value, 
	placeholder, 
	name, 
	valid, 
	label, 
	onChange, 
	onBlur, 
	inpWidth 
}) => {
	const SVG = ChooseInput(label);
	return (
		<InputField
			inpWidth = {inpWidth}
			valid = {valid}
			className = "page-login-field  Input"
		>
			<SVG
				size = "15px"
				name = "InputSVG"
			/>
			<input
				name = {name}
				value = {value}
				type = {type || "text"}
				placeholder = {placeholder}
				onChange = {onChange}
				onBlur = {onBlur}
				autoComplete = "off"
			/>
		</InputField>
	);
}

Input.propTypes = {
	inpWidth: propTypes.number,
	label: propTypes.string,
	name: propTypes.string,
	onBlur: propTypes.func,
	onChange: propTypes.func,
	placeholder: propTypes.string,
	type: propTypes.string,
	valid: propTypes.bool,
	value: propTypes.string
};

export default Input;
