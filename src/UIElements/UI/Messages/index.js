import styled from "styled-components";

import {PopUpMessage, toast as callPopUpMessage} from "./PopUpMessage";

const ErrorMessage = styled.p`
    height: 13px;
    padding: 10px 0;
    color: red;
    text-align: end;
    font-size: 12px;
    visibility: ${props => props.children.length ? "visible" : "hidden"};
`;

export {
	ErrorMessage,
	PopUpMessage,
	callPopUpMessage
};
