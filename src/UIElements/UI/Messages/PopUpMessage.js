import React from "react";
import propTypes from "prop-types";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const PopUpMessage = ({ time, position }) =>  (
	<ToastContainer
		position = {`top-${position || "right"}`}
		autoClose = {time || 3000}
		hideProgressBar = {false}
		newestOnTop = {false}
		closeOnClick
		rtl = {false}
		pauseOnVisibilityChange
		draggable
		pauseOnHover
	/>
);

PopUpMessage.propTypes = {
	position: propTypes.string,
	time: propTypes.number
};

export {
	PopUpMessage,
	toast
};
