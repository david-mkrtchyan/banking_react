import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    margin: 10px 0;
    .MainTitle{
        padding: 5px 2.2%;
        font-size: 20px;
        color: white;
        margin-bottom: 15px!important;
    }
`;