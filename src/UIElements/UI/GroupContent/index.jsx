import React from "react";

import { Container } from "./styledGroup";

const GroupContent = ({ children, styled, mainTitle, className }) => (
    <Container props = {styled} className = {className} >
        <p className = "MainTitle"> {mainTitle} </p>
        {children}
    </Container>
);

export default GroupContent;
