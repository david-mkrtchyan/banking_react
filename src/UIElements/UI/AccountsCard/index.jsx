import React from "react";
import { Link } from "react-router-dom";
import { ArrowSVG } from "SVG";
import propTypes from "prop-types";
import ChooseCurrency from "Engine/UI/Currency";

import "./styles.scss";

const AccountsCard = ({
    valute,
    money,
    to,
    title = false
}) => {
    let CurrencySVG;
    if(!title){
        CurrencySVG = ChooseCurrency(valute);
    } 
    return (
        <Link className = "default-link default-link-send"  to = {to} alt="img"> 
            {title ?
                <>
                    <h1 className = "color-blue title-left-text">
                        {title}
                    </h1>
                    <ArrowSVG
                        size = "32px"
                        name = "ArrowSVG"
                        className = "fal"
                    />
                </>
            :  
                <>
                    <h1 className = "color-blue left-text top-5">
                        {valute} Account
                    </h1>
                    <p className = "color-blue uppercase ultrabold">
                        <CurrencySVG
                            size = "18px"
                            name = "CurrencySVG"
                        />
                        <span>{money}.
                            <span style = {{fontSize: "17px"}}>00</span>
                        </span>  
                    </p> 
                    <ArrowSVG
                        size = "36px"
                        name = "ArrowSVG"
                        className = "falSend"
                    />
                </>
            }
        </Link>
    );
};

AccountsCard.propTypes = {
    valute: propTypes.string,
    money: propTypes.string,
    to: propTypes.string
};

export default AccountsCard;
