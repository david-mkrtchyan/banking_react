import styled from "styled-components";

export const Content = styled.div`
  width: 100%;
  min-height: ${props => props.height ? `${window.innerHeight - props.height}px` : "unset"};
  display: flex;
  align-items: center;
  justify-content: center;
`;
