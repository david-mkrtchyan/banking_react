/*eslint-disable jsx-a11y/anchor-is-valid*/
import React, { useState } from "react";
import PhoneField from "UI/PhoneField";

//Style
import "./styledAccordion.scss";

const AccordionItem = ({ 
		children, 
		title, 
		classItem, 
		List = [], 
		currencyCount, 
		handleCurrencyCount,
		formData,
		handlePhoneChange,
		handleSendCurrency
	}) => {
	const [openedView, setOpenedView] = useState(false);
	
	const handleView = () => {
		setOpenedView(!openedView);
	};

	const mapItemsNavBar = list => list.map(item => (
		<div 
			className = "accordion-item__inner navigation-down-list-item" 
			key = {item.id}
		>
			<div className = "accordion-item__content navigation-down-list-wrapper">
				<div className = "accordion-item__paragraph dashboard-navigation-down-item">
					<div className = "accordion-item__paragraph__input">
						<input 
							type = "text" 
							placeholder = "Money" 
							className = "accordion-item__paragraph__input input" 
							value = {currencyCount}
							onChange = {handleCurrencyCount}
						/>
						<span> $ </span>
					</div>
					<div className = "accordion-item__paragraph__address">
						<PhoneField
                  			label = ""
                  			value={formData.phone_number.value}
                  			valid={formData.phone_number.valid}
                  			onChange={handlePhoneChange}
                		/>
					</div>
					<button className = "accordion-item__paragraph__button"  onClick = {handleSendCurrency}>
						{item.title}
					</button>
				</div>
			</div>
		</div>
	));

	return (
		<div
			className = {`accordion-item, dashboard-navigation-block ${openedView && "accordion-item--opened"}`}
			
		>
			
			<div className = "dashboard-navigation-item accordion-item__line" style = {{display: 'flex'}}>
				{children}
				<i className = {`dashboard-icons ${classItem}`} />
				<span className = "accordion-item__title accordion-item_button" onClick = {handleView}>{title}</span>
			</div>
			{
				!List ?
					null : (
						<>
							{ mapItemsNavBar(List) }
						</>
					)
			}
		</div>
	);
};

const Accordion = ({children, List, currencyCount, handleCurrencyCount, address, handleSetAddress, formData, handlePhoneChange, handleSendCurrency }) => (
	<ul className = "accordion-list dashboard-navigation-List">
        {List.map(item => (
            <li className = "accordion-list__item dashboard-navigation-items" key = {item.id}>
                <AccordionItem {...{...item, children, currencyCount, handleCurrencyCount, address, handleSetAddress, formData, handlePhoneChange, handleSendCurrency }} />
            </li>
        ))}
	</ul>
);

export default Accordion;
