import React from "react";
import { NavLink, useHistory } from "react-router-dom";

import "./styles.scss";
import { Container, ModalHeader } from "./StyledModal";
import Logo from "~c/includes/Logo";

const Modal = ({ title, linkText, children, width = 300, to, LogoWidth }) => {
	const history = useHistory();

	const handleGoBack = (e) => {
		const {name} = e.target.dataset;

		if (name !== "modalback") return;
		history.goBack();
	};

	return (
		<>
			{
				!to ?
					(
						<Container width = {width} className = "Modal">
							<ModalHeader className = "modalHeader">
								<Logo 
									width = {LogoWidth}
									className = "LogoModal"
								/>
								<h2 className = "TitleModal" >{title}</h2>
								<p>
									<NavLink to = "/login">{linkText}</NavLink>
								</p>
							</ModalHeader>
							<div className = "container">
								{children}
							</div>
						</Container>
					)
					:
					(
						<div className = "Modal_backround" data-name = "modalback" onClick = {handleGoBack}>
							<Container width = {width} className = "Modal">
								<ModalHeader className = "modalHeader">
									<Logo 
										width = {LogoWidth}
										className = "LogoModal"
									/>
									<h2 className = "TitleModal" >{title}</h2>
									<p>
										<NavLink to = "/login">
											{linkText}
										</NavLink>
									</p>
								</ModalHeader>
								<div className = "container">
									{children}
								</div>
							</Container>
						</div>
					)
			}
		</>
	);
};

export default Modal;
