import styled from "styled-components";

export const Container = styled.div`
  width: ${props => props.width}px;
  margin: 0 auto;
  border-radius: 6px;
`;

export const ModalHeader = styled.div`
  background-color: transparent;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  display: flex;
  flex-direction: column;
  justify-content:center; 
  align-items:center;
  .TitleModal{
    margin: 20px 0;
    font-weight: 800!important;
    font-size: 24px;
    line-height: 26px; 
  }
`;
