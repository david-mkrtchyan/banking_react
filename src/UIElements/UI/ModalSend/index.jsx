import React from "react";

import "./StyledModal.css";

const Modal = ({ 
    money, 
    amountName, 
    user_name, 
    email, 
    handleSendAnotherUser,
    displayModal,
    closeModal
})  => {
  
  const divStyle = {
    display: displayModal ? 'flex' : 'none',
  };

    return (
      <div 
        className="modal"
        onClick={ closeModal }
        style={divStyle}>

        <div className="modal-content"
          onClick={ e => e.stopPropagation() }>
          <div className="modal-flex">
				<div className = "Content_Body">
					<p> You want send {money} {amountName} to {user_name}</p>
					<p> {email} </p>
				</div>
				<div className = "ButtonBlock">
					<button className = "Right" onClick= {handleSendAnotherUser}> Yes </button>
					<button className = "Wrong" onClick={ closeModal }> No </button> 
				</div>
          </div>

        </div>

      </div>
    );
}

export default Modal;
