import React from "react";
import { EditSVG } from "SVG";

import "./styled.scss";

const TextAreaMessage = ({
    name,
    onChange,
    value
}) => (
    <div className = "TextAreaMessage">
        <EditSVG 
            name = "EditSVG"
            size = "18px"
        />
        <textarea
            onChange = {onChange}
            placeholder = "Message..."
            name = {name}
            value = {value}
            cols = {373}
            rows = {20}
        /> 
    </div>
);

export default TextAreaMessage;