import React, {useEffect, useRef} from "react";
import propTypes from "prop-types";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/dist/style.css";

//Style
import { Container, InputField } from "./styledPhoneField";

const PhoneField = ({
	value, 
	onChange, 
	onBlur, 
	isValid, 
	valid
}) => {
	const input = useRef(null);

	useEffect(() => {
		input.current.numberInputRef.setAttribute("autoComplete", "off")
	}, [])

	return (
		<Container className = "PhoneField">
			<InputField valid = {valid}>
				<PhoneInput
					value = {value}
					onChange = {onChange.bind(this, "phone_number")}
					onBlur = {onBlur}
					className = "PhoneInput"
					defaultCountry = "us"
					inputClass = "inputField"
					isValid = {isValid}
					ref = {input}
				/>
			</InputField>
		</Container>
	);
};

PhoneField.propTypes = {
	isValid: propTypes.bool,
	label: propTypes.string,
	onBlur: propTypes.func,
	onChange: propTypes.func,
	valid: propTypes.bool,
	value: propTypes.string
};

export default PhoneField;
