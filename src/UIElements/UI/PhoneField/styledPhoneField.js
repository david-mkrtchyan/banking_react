import styled from "styled-components";

export const Container = styled.div`
    height: 40px;
    margin: 10px 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    .country-list{
        margin: 0 !important;
        background-color: #191a1c !important;
        width: 270px;
        max-height: 170px !important;
        .flag-option{
            color: #aaaaaa;
            .country-flag{
                display:flex;
                width: 100%;
                height: auto !important;
            }
        }
    }
    .phoneLabel {
        font-size: 18px !important;
    }
    @media screen and (max-width: 320px){
        .country-list{
            width: 250px !important;
        }
    }
`;

export const InputField = styled.div`
    width: 100%;
    height: 100%;
    border-radius: 5px;
    .form-control{
        width: 100% !important;
    }
    .inputField {
        height: 100% !important;
    }
    .react-tel-input{
        height: 100% !important;
        width: 100% !important;
        height:45px;
        margin-bottom:10px;
        #phone-form-control{
            font-size:12px;
            height:45px;
            border-bottom:solid 0px rgba(0,0,0,0.1);
            background-color:#191a1c;
            display:block;
            width:100%;
            padding-left: 74px;
            border: none;
            &:focus ~em{color:#000000; opacity:0;}
        }
    }
    
    #flag-dropdown{
        background-color:#191a1c;
        border: none;
        padding: 0 15px;
        border-radius: 30px 0 0 30px;
        border-right: 1px solid rgba(255,255,255,0.1);
        .selected-flag{
            background: transparent; 
        }
        &:hover .selected-flag{
            background-color: transparent !important;
        }
    }
    
`;
