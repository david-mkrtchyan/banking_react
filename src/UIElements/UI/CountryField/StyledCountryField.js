import styled from "styled-components";

export const Container = styled.div`
    height: 40px;
    margin: 10px 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const InputField = styled.div`
    height: 100%;
    border-color: ${props => (props.valid !== null) && "red"};
    border-color: ${props => props.valid && "green"};
    border-radius: 5px;
    position: relative;
    #InputSVG{
        position: absolute;
        z-index: 1;
        top: 27%;
        left: 4.5%;
    }
    .flag-select {
        width:100%;
        padding-bottom:0;
        margin-bottom: 10px;
        
        .selected--flag--option {
            text-align: center;
            font-size: 12px;
            background-color: #191a1c;
            display: block;
            border-radius: 30px;
            height: 40px;
            display:flex;
            padding-left: 40px;
            align-items: center;
            .country-label{
                padding:0;
                color: #aaaaaa;
                font-size: 12px;
            }
            .country-flag{
                width: auto !important;
                height: auto !important;
                display:flex;
                margin-right: 10px;
                img{
                    margin-right:10px;
                }
            }
        }
        .flag-options{
            background-color: #191a1c;
            width: 100%;
            .flag-option{
                color: #aaaaaa;
                .country-flag{
                    display:flex;
                    width: 100%;
                    height: auto !important;
                }
            }
        }
    }
`;
