import React from "react";
import propTypes from "prop-types";
import ReactFlagsSelect from "react-flags-select";
import { LocationSVG } from "SVG_Input";
import "react-flags-select/css/react-flags-select.css";

//Style
import { InputField } from "./StyledCountryField";
import "./styles.scss";

const CountryField = ( { onChange, valid, country } ) => (
		<InputField valid = {valid} >
			<LocationSVG 
				size = "15px"
				name = "InputSVG"
			/>
			<ReactFlagsSelect
				defaultCountry = {country}
				onSelect = {onChange.bind(this, "country_code")}
				id = "country-form-control" 
			/>
		</InputField>
);

CountryField.propTypes = {
	label: propTypes.string,
	onChange: propTypes.func,
	valid: propTypes.bool
};

export default CountryField;
