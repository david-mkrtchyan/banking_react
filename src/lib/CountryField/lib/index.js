'use strict';

import React from "react";
// var React = require('react');
import propTypes from 'prop-types';

import _countries from './countries';


var ReactFlagsSelect = function (_Component) {
	function ReactFlagsSelect(props) {

		var _this = _Component.call(this, props);

		var defaultCountry = _countries2.default[_this.props.defaultCountry] && _this.props.defaultCountry;

		_this.state = {
			openOptions: false,
			defaultCountry: defaultCountry,
			filteredCountries: []
		};

		_this.toggleOptions = _this.toggleOptions.bind(_this);
		_this.closeOptions = _this.closeOptions.bind(_this);
		_this.onSelect = _this.onSelect.bind(_this);
		_this.filterSearch = _this.filterSearch.bind(_this);
		_this.setCountries = _this.setCountries.bind(_this);
		return _this;
	}

	ReactFlagsSelect.prototype.toggleOptions = function toggleOptions() {
		!this.state.disabled && this.setState({
			openOptions: !this.state.openOptions
		});
	};

	ReactFlagsSelect.prototype.toggleOptionsWithKeyboard = function toggleOptionsWithKeyboard(evt) {
		evt.preventDefault();
		if (evt.keyCode === 13) {
			//enter key: toggle options
			this.toggleOptions();
		} else if (evt.keyCode === 27) {
			//esc key: hide options
			!this.state.disabled && this.setState({
				openOptions: false
			});
		}
	};

	ReactFlagsSelect.prototype.closeOptions = function closeOptions(event) {
		if (event.target !== this.refs.selectedFlag && event.target !== this.refs.flagOptions && event.target !== this.refs.filterText) {
			this.setState({
				openOptions: false
			});
		}
	};

	ReactFlagsSelect.prototype.onSelect = function onSelect(countryCode) {
		this.setState({
			selected: countryCode,
			filter: ''
		});
		this.props.onSelect && this.props.onSelect(countryCode);
	};

	ReactFlagsSelect.prototype.onSelectWithKeyboard = function onSelectWithKeyboard(evt, countryCode) {
		evt.preventDefault();
		if (evt.keyCode === 13) {
			//enter key: select
			this.onSelect(countryCode);
			this.closeOptions(evt);
		} else if (evt.keyCode === 27) {
			//esc key: hide options
			this.toggleOptions();
		}
	};

	ReactFlagsSelect.prototype.updateSelected = function updateSelected(countryCode) {
		var isValid = _countries2.default[countryCode];

		isValid && this.setState({
			selected: countryCode
		});
	};

	ReactFlagsSelect.prototype.filterSearch = function filterSearch(evt) {
		var _this2 = this;

		var filterValue = evt.target.value;
		var filteredCountries = filterValue && this.state.countries.filter(function (key) {
			var label = _this2.props.customLabels[key] || _countries2.default[key];
			return label && label.match(new RegExp(filterValue, 'i'));
		});

		this.setState({ filter: filterValue, filteredCountries: filteredCountries });
	};

	ReactFlagsSelect.prototype.setCountries = function setCountries() {
		var _this3 = this;

		var fullCountries = Object.keys(_countries2.default);

		var selectCountries = this.props.countries && this.props.countries.filter(function (country) {
			return _countries2.default[country];
		});

		//Filter BlackList
		if (this.props.blackList && selectCountries) {
			selectCountries = fullCountries.filter(function (countryKey) {
				return selectCountries.filter(function (country) {
					return countryKey === country;
				}).length === 0;
			});
		}

		this.setState({
			countries: selectCountries || fullCountries
		}, function () {
			var selected = _this3.state.selected;


			if (selected && !_this3.state.countries.includes(selected)) {
				_this3.setState({ selected: null });
			}
		});
	};

	ReactFlagsSelect.prototype.componentDidMount = function componentDidMount() {
		this.setCountries();
		!this.props.disabled && window.addEventListener("click", this.closeOptions);
	};

	ReactFlagsSelect.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
		if (prevProps.countries !== this.props.countries || prevProps.blackList !== this.props.blackList) {
			this.setCountries();
		}
	};

	ReactFlagsSelect.prototype.componentWillUnmount = function componentWillUnmount() {
		!this.props.disabled && window.removeEventListener("click", this.closeOptions);
	};

	ReactFlagsSelect.prototype.render = function render() {
		var _this4 = this;

		var isSelected = this.state.selected || this.state.defaultCountry;
		var selectedSize = this.props.selectedSize;
		var optionsSize = this.props.optionsSize;
		var alignClass = this.props.alignOptions.toLowerCase() === 'left' ? 'to--left' : '';

		return React.default.createElement(
			'div',
			{ className: 'flag-select ' + (this.props.className ? this.props.className : "") },
			React.default.createElement(
				'div',
				{ ref: 'selectedFlag', style: { fontSize: selectedSize + 'px' }, className: 'selected--flag--option ' + (this.props.disabled ? 'no--focus' : ''), tabIndex: '0', onClick: this.toggleOptions, onKeyUp: function onKeyUp(evt) {
						return _this4.toggleOptionsWithKeyboard(evt);
					} },
				isSelected && React.default.createElement(
					'span',
					{ className: 'country-flag', style: { width: selectedSize + 'px', height: selectedSize + 'px' } },
					React.default.createElement('img', { src: require('IMG/flags/' + isSelected.toLowerCase() + '.svg'), alt: isSelected }),
					this.props.showSelectedLabel && React.default.createElement(
						'span',
						{ className: 'country-label' },
						this.props.customLabels[isSelected] || _countries2.default[isSelected]
					)
				),
				!isSelected && React.default.createElement(
					'span',
					{ className: 'country-label' },
					this.props.placeholder
				),
				React.default.createElement(
					'span',
					{ className: 'arrow-down ' + (this.props.disabled ? 'hidden' : '') },
					'\u25BE'
				)
			),
			this.state.openOptions && React.default.createElement(
				'div',
				{ ref: 'flagOptions', style: { fontSize: optionsSize + 'px' }, className: 'flag-options ' + alignClass },
				this.props.searchable && React.default.createElement(
					'div',
					{ className: 'filterBox' },
					React.default.createElement('input', { type: 'text', placeholder: this.props.searchPlaceholder, ref: 'filterText', onChange: this.filterSearch })
				),
				(this.state.filter ? this.state.filteredCountries : this.state.countries).map(function (countryCode) {
					return React.default.createElement(
						'div',
						{ className: 'flag-option ' + (_this4.props.showOptionLabel ? 'has-label' : ''), key: countryCode, tabIndex: '0', onClick: function onClick() {
								return _this4.onSelect(countryCode);
							}, onKeyUp: function onKeyUp(evt) {
								return _this4.onSelectWithKeyboard(evt, countryCode);
							} },
						React.default.createElement(
							'span',
							{ className: 'country-flag', style: { width: optionsSize + 'px', height: optionsSize + 'px' } },
							React.default.createElement('img', { src: require('IMG/flags/' + countryCode.toLowerCase() + '.svg') }),
							_this4.props.showOptionLabel && React.default.createElement(
								'span',
								{ className: 'country-label' },
								_this4.props.customLabels[countryCode] || _countries2.default[countryCode]
							)
						)
					);
				})
			)
		);
	};

	return ReactFlagsSelect;
}(React.Component);

ReactFlagsSelect.defaultProps = {
	selectedSize: 16,
	optionsSize: 14,
	placeholder: "Select a country",
	showSelectedLabel: true,
	showOptionLabel: true,
	alignOptions: "right",
	customLabels: {},
	disabled: false,
	blackList: false,
	searchable: false,
	searchPlaceholder: 'Search'
};

ReactFlagsSelect.propTypes = process.env.NODE_ENV !== "production" ? {
	countries: propTypes.default.array,
	blackList: propTypes.default.bool,
	customLabels: propTypes.default.object,
	selectedSize: propTypes.default.number,
	optionsSize: propTypes.default.number,
	defaultCountry: propTypes.default.string,
	placeholder: propTypes.default.string,
	className: propTypes.default.string,
	showSelectedLabel: propTypes.default.bool,
	showOptionLabel: propTypes.default.bool,
	alignOptions: propTypes.default.string,
	onSelect: propTypes.default.func,
	disabled: propTypes.default.bool,
	searchable: propTypes.default.bool,
	searchPlaceholder: propTypes.default.string
} : {};

export default ReactFlagsSelect;
module.exports = exports['default'];