const path = require("path");

const aliasPaths = {
	"~": path.join(__dirname, "../src"),
	"~c": path.join(__dirname, "../src/components"),
	"Container": path.join(__dirname, "../src/container"),
	"Pages": path.join(__dirname, "../src/pages"),
	"SVG": path.join(__dirname, "../src/UIElements/SVG/wrapperSVG"),
	"SVG_Currency": path.join(__dirname, "../src/UIElements/SVG/wrapperSVG_currency"),
	"SVG_Input": path.join(__dirname, "../src/UIElements/SVG/wrapperSVG_input"),
	"Engine": path.join(__dirname, "../src/Engine"),
	"IMG": path.join(__dirname, "../src/assets/img"),
	"UTILS": path.join(__dirname, "../src/utils"),
	"Libs": path.join(__dirname, "../src/lib"),
	"HOC": path.join(__dirname, "../src/HOC"),

	"UI": path.join(__dirname, "../src/UIElements/UI"),
	"Data": path.join(__dirname, "../src/data"),
	"Routes": path.join(__dirname, "../src/Route/Routes.js"),
	
	"HTTP": path.join(__dirname, "../src/server/URL.js"),
	"Method": path.join(__dirname, "../src/server/methods"),

	"BrowserStorage": path.join(__dirname, "../src/Engine/BrowserStorage"),

	"Constants": path.join(__dirname, "../src/store/constants"),
	"Reducer": path.join(__dirname, "../src/store/reducers"),
	"Action": path.join(__dirname, "../src/store/actions"),

	"Style": path.join(__dirname, "../src/assets/scss"),
	"StylesPage": path.join(__dirname, "../src/assets/scss/pages"),
	"GlobalStyle": path.join(__dirname, "../src/assets/scss/main.scss"),
	"UTILS_Style": path.join(__dirname, "../src/assets/scss/utils/index.scss"),
	"Media": path.join(__dirname, "../src/assets/scss/media")
};

module.exports = aliasPaths;
